
public class AnimalFamilier {
	private int age;
    private double poids;
    private double taille;
    private String couleur;
    
    public void initialiser(int page, double ppoids, double ptaille, String pcouleur) {
    	this.setAge(page);
        this.setPoids(ppoids);
        this.setTaille(ptaille);
        this.setCouleur(pcouleur);
    }
    
    public void manger() {
    	System.out.println("J'ai si faim!!!... Donne-moi un biscuit!");
    }
    
    @Override
	public String toString() {
		return "Caractéristiques : [age=" + age + ", poids=" + poids + ", taille=" + taille + ", couleur=" + couleur + "]";
	}

	public void dormir() {
    	System.out.println("Bonne nuit, a demain!");
    }
    
    public String dire(String mot) {
    	String res;
    	res = "Ok!! Ok!! "+mot;
    	return res;
    }

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getPoids() {
		return poids;
	}

	public void setPoids(double poids) {
		this.poids = poids;
	}

	public double getTaille() {
		return taille;
	}

	public void setTaille(double taille) {
		this.taille = taille;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

}
