
public class Commercial extends Employe{
	private final static int POURCENTCA = 1;
	private int chiffreAffaire;
	int fixe;
	
	public Commercial(String pnom, int pCA, int pfixe) {
		super(pnom);
		this.setInfoSalaire(pCA,pfixe);
	}
	
	public void setInfoSalaire(int pCA, int pfixe) {
		this.chiffreAffaire = pCA;
		this.fixe = pfixe;
	}
	
	public void afficher() {
		super.afficher();
		System.out.println("Chiffre d'affaire : "+this.chiffreAffaire);
		System.out.println("Montant fixe : "+this.fixe);
	}
	
	public int getSalaire() {
		int res;
		res = this.fixe +(POURCENTCA/100)*this.chiffreAffaire;
		return res;
	}

}
