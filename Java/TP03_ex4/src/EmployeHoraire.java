
public class EmployeHoraire extends Employe {
	private final static int NBHEURESEMAINE = 35;
	private int pourcentageHeurSup;
	private int nbHeure;
	private int tarifHeure;
	
	public EmployeHoraire(String nom, int pnbHeure, int ppourcentageHeurSup, int pTH) {
		super(nom);
		this.setInfoSalaire(pnbHeure, ppourcentageHeurSup, pTH);
	}
	
	public void setInfoSalaire(int pnbHeure, int ppourcentageHeurSup, int pTH) {
		this.nbHeure=pnbHeure;
		this.pourcentageHeurSup=ppourcentageHeurSup;
		this.tarifHeure=pTH;
	}
	
	public int getSalaire() {
		int nbHeureSup;
		int salaire;
		if(this.nbHeure > NBHEURESEMAINE) {
			nbHeureSup = this.nbHeure - NBHEURESEMAINE;
			salaire = NBHEURESEMAINE * tarifHeure + (nbHeureSup*tarifHeure*(1+this.pourcentageHeurSup/100));
		}else {
			salaire = this.nbHeure * this.tarifHeure;
		}
		return salaire;
	}
	
	public void afficher() {
		System.out.println("Poucentage � appliquer aux heures supp : "+this.pourcentageHeurSup);
		System.out.println("a fait "+this.nbHeure+" heures");
		System.out.println("sera pay� "+this.tarifHeure+" euros de l'heure");
	}

}
