
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int MAX = 10;
		Employe employes[] = new Employe[MAX];
		int nbEmployes;
		
		nbEmployes = 0;
		Employe employe1 = new Commercial("Dupond",20000,1300);
		employes[nbEmployes]=employe1;
		nbEmployes +=1;
		Employe employe2 = new Commercial("Duprat",30000,1500);
		employes[nbEmployes]=employe2;
		nbEmployes +=1;
		Employe employe3 = new Commercial("Mercier",50000,3000);
		employes[nbEmployes]=employe3;
		nbEmployes +=1;
		Employe employe4 = new EmployeHoraire("Tutu",25,40,20);
		employes[nbEmployes]=employe4;
		nbEmployes +=1;
		Employe employe5 = new EmployeHoraire("Toto",20,43,25);
		employes[nbEmployes]=employe5;
		nbEmployes +=1;
		for(int i=1;i<nbEmployes;i++) {
			System.out.println(employes[i].getNom()+" gagne "+employes[i].getSalaire()+" � ");
		}

	}

}
