
public class Compte {
	private int numeroCompte;
	private double solde;
	
	
	public int getNumeroCompte() {
		return numeroCompte;
	}
	public void setNumeroCompte(int numeroCompte) {
		this.numeroCompte = numeroCompte;
	}
	public double getSolde() {
		return solde;
	}
	public void setSolde(double solde) {
		this.solde = solde;
	}
	
	public void compte(int pnum) {
		System.out.println("Numero compte : "+this.numeroCompte);
		System.out.println("solde : "+this.solde);
	}
	
	public void crediter(double pmontant) {
		this.solde += pmontant;
	}
	
	public void dediter(double pmontant) {
		this.solde -= pmontant;
	}
	
	public void afficher() {
		System.out.println("Numero compte : "+this.numeroCompte);
		System.out.println("solde : "+this.solde);
	}
}
