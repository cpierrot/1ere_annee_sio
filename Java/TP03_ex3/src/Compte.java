
public class Compte {
        private int numeroCompte;
        private double solde;
        private static int dernierNumCompte = 0;


        public int getNumeroCompte()
        {
            return numeroCompte;
        }
        public void setNumeroCompte(int numeroCompte)
        {
            this.numeroCompte = numeroCompte;
        }
        public double getSolde()
        {
            return solde;
        }
        public void setSolde(double solde)
        {
            this.solde = solde;
        }

        public Compte(int pnum)
        {
            this.numeroCompte = pnum;
            this.solde = 0;
            dernierNumCompte++;
            
        }

        public void crediter(double pmontant)
        {
            this.solde += pmontant;
            System.out.println("Nouveau solde : " + this.solde);
        }

        public void dediter(double pmontant)
        {
            this.solde -= pmontant;
            System.out.println("Nouveau solde : " + this.solde);
        }

        public void afficher()
        {
            System.out.println("Numero compte : " + this.numeroCompte);
            System.out.println("solde : " + this.solde);
        }

}
