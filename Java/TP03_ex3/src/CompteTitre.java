
public class CompteTitre extends Compte {
	private double pourcentdroitgarde;

    
    public CompteTitre(double pdroitg, int pnum, double psolde)  super( pnum)
    {
        this.pourcentdroitgarde = pdroitg;
    }

    public void payerlesdroits()
    {
        double droit = pourcentdroitgarde * this.getSolde();
        this.dediter(droit);
    }

    public void setDroitsgarde(double pdroit)
    {
        this.pourcentdroitgarde = pdroit;
    }

    public double GetDroitsgarde()
    {
        return this.pourcentdroitgarde;
    }

    public void afficher()
    {
        super.afficher();
    }

}
