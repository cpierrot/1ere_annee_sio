
public class Article {
	private final static double TVA=19.6;
	private int reference;
	private String intutile;
	private double prixHT;
	private int quantiteStock;
	
	
	
	public int getReference() {
		return reference;
	}
	public void setReference(int reference) {
		this.reference = reference;
	}
	public String getIntutile() {
		return intutile;
	}
	public void setIntutile(String intutile) {
		this.intutile = intutile;
	}
	public double getPrixHT() {
		return prixHT;
	}
	public void setPrixHT(double prixHT) {
		this.prixHT = prixHT;
	}
	public int getQuantiteStock() {
		return quantiteStock;
	}
	public void setQuantiteStock(int quantiteStock) {
		this.quantiteStock = quantiteStock;
	}
	
	
	public void initialiser(int preference, String pintutile, double pprix, int pquantite) {
		this.reference = preference;
		this.intutile = pintutile;
		this.prixHT = pprix;
		this.quantiteStock = pquantite;
	}
	
	public void approvisionner(int nbUnite) {
		this.quantiteStock = this.quantiteStock + nbUnite;
	}
	
	public boolean vendre(int pNbUnit) {
		boolean res;
		if(pNbUnit > this.quantiteStock) {
			res = false;
		}else {
			this.quantiteStock = this.quantiteStock - pNbUnit;
			res = true;
		}
		return res;			
	}
	
	public double prixTTC() {
		return this.prixHT + this.prixHT * (this.TVA/100);
	}
	
	public void afficher() {
		System.out.println(this.reference +" "+ this.intutile +" � "+ this.prixHT+"� HT");
	}
	
	public boolean equals(Article pArticle) {
		boolean res;
		if(this.reference == pArticle.reference) {
			res = true;
		}else {
			res = false;
		}
		return res;
	}
	

}
