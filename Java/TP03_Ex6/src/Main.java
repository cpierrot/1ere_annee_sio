
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Animal unAnimal = new Chauvesouris();
		unAnimal.deplace();
		unAnimal.mange();
		System.out.println(unAnimal.getClass().getName());
		
		Oiseau unOiseau = new Chauvesouris();
		System.out.println(unOiseau.getClass().getName());
		unOiseau.deplace();
		unOiseau.mange();
		unOiseau.vole();
		
		Chauvesouris uneChauvesouris = new Chauvesouris();
		System.out.println(uneChauvesouris.getClass().getName());
		uneChauvesouris.allaite();
		
		Chauvesouris uneAutre = (Chauvesouris)unAnimal;
		System.out.println(uneAutre.getClass().getName());
		uneAutre.allaite();
		uneAutre.deplace();
		uneAutre.vole();
	}

}
