
public class Chauvesouris implements Mammifere, Oiseau{
	public void deplace() {
		System.out.println("Il bouge");
	}
	
	public void mange() {
		System.out.println("Il mange");
	}
	
	public void allaite() {
		System.out.println("Il allaite");
	}
	public void vole() {
		System.out.println("Il vole");
	}
}
