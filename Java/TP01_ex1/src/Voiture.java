
public class Voiture {
	private String nom;
	private String marque;
	private String couleur;
	private int kilometrage;
	
	public void initialiser(String pnom, String pmarque, String pcouleur, int pkilometrage) 
	{
		this.nom = pnom;
		this.marque = pmarque;
		this.couleur = pcouleur;
		this.kilometrage = pkilometrage;
	}
	
	public void demarrer()
	{
		System.out.println("Attachez vos ceintures ! je demarre ...");
	}
	
	public void arreter()
	{
		System.out.println("STOP!");
	}
	
	public int rouler(int pduree)
	{
		int res;
		res = pduree * 60;
		this.kilometrage = this.kilometrage + res;
		return res;
	}
	
	public void afficher()
	{
		System.out.println("nom : "+this.nom);
		System.out.println("marque : "+this.marque);
		System.out.println("couleur : "+this.couleur);
		System.out.println("kilometrage : "+this.kilometrage);
	}
	
	@Override
	public String toString() {
		return "Voiture [nom=" + nom + ", marque=" + marque + ", couleur=" + couleur + ", kilometrage=" + kilometrage
				+ "]";
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getMarque() {
		return marque;
	}
	public void setMarque(String marque) {
		this.marque = marque;
	}
	
	public String getCouleur() {
		return couleur;
	}
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	public int getKilometrage() {
		return kilometrage;
	}
	public void setKilometrage(int kilometrage) {
		this.kilometrage = kilometrage;
	}
	
	
}
