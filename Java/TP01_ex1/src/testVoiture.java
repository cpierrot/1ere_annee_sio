
public class testVoiture {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Voiture maVoiture =  new Voiture();
		maVoiture.initialiser("Jackie", "Tesla", "rouge", 20000);
		
		maVoiture.afficher();
		maVoiture.demarrer();
		
		//Appel d'une fonction --> on r�cup�re le r�sultat
		int distance = maVoiture.rouler(2);
		System.out.println("Distance parcourue : "+distance);
		
		int kilometrage = maVoiture.getKilometrage();
		System.out.println("Kilometrage de la voiture : "+ kilometrage);
		
		maVoiture.afficher();
		maVoiture.arreter();
		
		/* Autre possibilit�
		String res = maVoiture.ToString();
		System.out.println(res);
				OU
		System.out.println(maVoiture.ToString();*/
	}

}
