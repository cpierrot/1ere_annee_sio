
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int MAX = 2;
		
		Magasin unMagasin = new Magasin();
		Electromenagers[] unTabElectro = new Electromenagers[MAX];
		Primeur[] unTabPrimeur = new Primeur[MAX];
		Article unArticle = new Electromenagers(40,50,"Micro-onde","Darty");
		Article unArticle2 = new Electromenagers(30,60,"Four","Darty");
		unTabElectro[0]=(Electromenagers)unArticle;
		unTabElectro[1]=(Electromenagers)unArticle2;
		unArticle= new Primeur(3,5.5,"Fraise","Carrefour");
		unArticle2= new Primeur(4,6,"Pomme","Carrefour");
		unArticle.remplirStock(15);
		unArticle2.remplirStock(30);
		unTabPrimeur[0]=(Primeur)unArticle;
		unTabPrimeur[1]=(Primeur)unArticle2;
		unMagasin.setTabElectro(unTabElectro);
		unMagasin.setTabPrimeur(unTabPrimeur);
		double prix = unArticle.vendre(30);
		System.out.println("Prix de la vente : "+prix);
		prix = unArticle2.vendre(10);
		unMagasin.afficher();
	}

}
