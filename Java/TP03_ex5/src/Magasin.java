
public class Magasin implements Description, TauxRendement {
	private double depense;
	private double revenus;
	private Electromenagers[] tabElectro;
	private Primeur[] tabPrimeur;
	private final static int MAX = 2;
	
	public Magasin() {
		this.tabElectro = new Electromenagers[MAX];
		this.tabPrimeur = new Primeur[MAX];
		this.depense = 0;
		this.revenus = 0;
	}
	
	public double calculerRendement() {
		double rendement = (this.revenus-this.depense)/this.depense;
		return rendement;
	}
	
	public void afficher() {
		System.out.println("Depenses : "+this.depense);
		System.out.println("Revenus: "+this.revenus);
		for(int i = 0; i<MAX; i++) {
			tabElectro[i].afficher();
		}
		for(int i = 0; i<MAX; i++) {
			tabPrimeur[i].afficher();
		}
	}
	
	public Electromenagers[] getTabElectro() {
		return this.tabElectro;
	}
	
	public void setTabElectro(Electromenagers[] ptab) {
		this.tabElectro = ptab;
	}
	public void setTabPrimeur(Primeur[] ptab) {
		this.tabPrimeur = ptab;
	}
}
