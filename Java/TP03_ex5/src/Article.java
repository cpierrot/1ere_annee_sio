
public class Article implements Description, TauxRendement{
	private double prixAchat;
	private double prixVente;
	private String nom;
	private String fournisseur;
	
	public Article(double unPrixAchat, double unPrixVente, String unNom, String unFournisseur) {
		this.prixAchat=unPrixAchat;
		this.prixVente=unPrixVente;
		this.nom=unNom;
		this.fournisseur=unFournisseur;
	}
	public double getPrixVente() {
		return this.prixVente;
	}
	
	public void setPrixVente(double pprix) {
		this.prixVente = pprix;
	}
	
	public double calculerRendement() {
		return(this.prixVente - this.prixAchat)/this.prixAchat;
	}
	
	public void remplirStock(double pquantite) {
	}
	
	public double vendre(double pquantiteVendu) {
		return 0;
	}
	
	public void afficher() {
		System.out.println("Le nom du produit: "+this.nom);
		System.out.println("Prix de Vente: "+this.prixVente);
		System.out.println("Prix d'Achat: "+this.prixAchat);
		System.out.println("Fournisseur: "+this.fournisseur);
		System.out.println("Le rendement: "+this.calculerRendement());
	}

}
