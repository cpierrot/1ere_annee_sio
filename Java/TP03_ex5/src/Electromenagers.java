
public class Electromenagers extends Article implements VendableParPiece, VendableEnSolde, Description {
	private double nbPiecesStock;
	
	public void remplirStock(double pquantite) {
		super.remplirStock(pquantite);
		this.nbPiecesStock+=pquantite;
	}

	public Electromenagers(double unPrixAchat, double unPrixVente, String unNom, String unFournisseur) {
		super(unPrixAchat, unPrixVente, unNom, unFournisseur);
		this.nbPiecesStock=0;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void LancerSolde(double pourcent) {
		// TODO Auto-generated method stub
		super.setPrixVente(super.getPrixVente()-(super.getPrixVente()*pourcent/100));
		
	}

	@Override
	public void terminerSolde(double ppourcent) {
		// TODO Auto-generated method stub
		super.setPrixVente(super.getPrixVente()/(1-(ppourcent/100)));
	}

	@Override
	public double vendre(double pquantiteVendu) {
		// TODO Auto-generated method stub
		double res = 0;
		if(this.nbPiecesStock>pquantiteVendu) {
			this.nbPiecesStock=this.nbPiecesStock - pquantiteVendu;
			res = pquantiteVendu*super.getPrixVente();
		}
		return res;
	}

}
