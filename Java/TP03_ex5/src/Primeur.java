
public class Primeur extends Article implements VendableParKg, Description {
	private double quantiteKilo;
	public Primeur(double unPrixAchat, double unPrixVente, String unNom, String unFournisseur) {
		super(unPrixAchat, unPrixVente, unNom, unFournisseur);
		this.quantiteKilo = 0;
		// TODO Auto-generated constructor stub
	}
	
	public void afficher() {
		super.afficher();
		System.out.println("Quantite(kilo) : " + this.quantiteKilo); 
	}
	
	public void remplirStock(double pquantite) {
		super.remplirStock(pquantite);
		this.quantiteKilo += pquantite;
	}
	
	@Override
	public double vendre(double pquantiteVendu) {
		// TODO Auto-generated method stub
		double res = 0;
		if(this.quantiteKilo>pquantiteVendu) {
			this.quantiteKilo -= pquantiteVendu;
			res = pquantiteVendu *super.getPrixVente();
		}
		return res;
	}
	
	
	

}
