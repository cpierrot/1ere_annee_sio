
public class Point {
	double abscisse;
	double ordonnee;
	
	public void initialiser(double px, double py) {
		this.abscisse = px;
		this.ordonnee = py;	
	}
	
	public void afficherCoord() {
		System.out.println(abscisse + " " + ordonnee);
	}
	
	public void deplacerPoint(double pdx, double pdy) {
		this.abscisse = pdx;
		this.ordonnee = pdy;
	}
	
	public double calculDistance(Point pPoint) {
		double res;
		res = Math.sqrt(Math.pow((pPoint.abscisse - this.abscisse), 2) + Math.pow((pPoint.ordonnee - this.ordonnee), 2));
		return res;
	}

}
