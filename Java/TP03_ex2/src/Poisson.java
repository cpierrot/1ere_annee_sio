
public class Poisson extends AnimalFamilier
{
	private double profondeur;
	
	public void init (int page, double ppoids, double ptaille, String pcouleur,double pprofondeur) 
	{
		super.initialiser(page, ppoids, ptaille, pcouleur);
		this.setProfondeur(pprofondeur);
		
	}
	public void plonger(double hauteur) 
	{
		System.out.println("Ce poisson va plonger de "+ hauteur +" profondeur.");
		this.profondeur = profondeur + hauteur;
		System.out.println("Ce poisson est � la profondeur"+ this.profondeur );
		
	}
	
	
	public double getProfondeur() {
		return profondeur;
	}
	public void setProfondeur(double profondeur) {
		this.profondeur = profondeur;
	}
}
