﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace M2L_JPO
{
    public partial class ligues : Form
    {
        public ligues()
        {
            InitializeComponent();
        }

        private void Ligue_Load(object sender, EventArgs e)
        {
            tbxLigue.Enabled = false;
            tbxDiscipline.Enabled = false;
            tbxAdresse.Enabled = false;
            btnEnregistrer.Enabled = false;

        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            tbxLigue.Enabled = true;
            tbxDiscipline.Enabled = true;
            tbxAdresse.Enabled = true;
            btnEnregistrer.Enabled = true;
        }

        private void btnNouveau_Click(object sender, EventArgs e)
        {
            tbxLigue.Enabled = true;
            tbxDiscipline.Enabled = true;
            tbxAdresse.Enabled = true;
            btnEnregistrer.Enabled = true;
        }
    }
}
