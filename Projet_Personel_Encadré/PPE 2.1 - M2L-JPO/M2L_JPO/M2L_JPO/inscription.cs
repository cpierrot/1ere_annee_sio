﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;

namespace M2L_JPO
{
    public partial class inscription : Form
    {
        public inscription()
        {
            InitializeComponent();
        }

        private void inscription_Load(object sender, EventArgs e)
        {
            
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data source=..\..\..\Base_de_données M2L-JPO.accdb");

            OleDbCommand cmd = new OleDbCommand("SELECT * FROM LIGUE", cnn);
            OleDbCommand equip = new OleDbCommand("SELECT * FROM EQUIPEMENT", cnn);

            OleDbDataReader dr;
            try
            {
                cnn.Open();
                dr = cmd.ExecuteReader
            (CommandBehavior.CloseConnection);

                while (dr.Read())
                {
                    //cbxLigue.Items.Add(dr["nom"]); 
                    cbxLigue.Items.Add(new MaClasse(Convert.ToInt32(dr["codeLigne"]), dr["nom"].ToString()));
                }
            }
            catch { }
            finally
            {
                cnn.Close();
            }


            try
            {
                cnn.Open();
                dr = equip.ExecuteReader
            (CommandBehavior.CloseConnection);

                while (dr.Read())
                {
                    cblEquipementTotal.Items.Add(dr["libelle"]);
                }
            }
            catch { }

            finally
            {
                cnn.Close();
            }

            
        }
        

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            cblEquipement.Items.Add(cblEquipementTotal.SelectedItem);
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data source=..\..\..\Base_de_données M2L-JPO.accdb");
            OleDbDataReader dr;
            OleDbCommand cmd = new OleDbCommand("insert into INSCRIPTION (longueur, largeur, codeLigne) select '" + tbxLongueur.Text + "',  '" + tbxLargeur.Text + "', codeLigne FROM LIGUE WHERE nom ='" + cbxLigue.SelectedItem+"' ; ", cnn);
            try
            {
                cnn.Open();
                dr = cmd.ExecuteReader
            (CommandBehavior.CloseConnection);

            }
            catch { }
            finally
            {
                cnn.Close();
            }

            /*
            OleDbCommand cmdEquip = new OleDbCommand("insert into DEMANDER (numInscription, codeEquipement) select numInscription FROM INSCRIPTION where codeLigne = " + Convert.ToInt32(((MaClasse)cbxLigue.SelectedItem).getValeur()) + ", codeEquipement FROM EQUIPEMENT where libelle = '" + cblEquipement.Items[0].ToString() + "' ; ", cnn);
            cnn.Open();
            dr = cmdEquip.ExecuteReader
        (CommandBehavior.CloseConnection);
            cnn.Close();*/

            MessageBox.Show("Inscription enregistrée avec succès");
        }

        private void cbxLigue_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(((MaClasse)cbxLigue.SelectedItem).getValeur());
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            cblEquipement.Items.Remove(cblEquipement.SelectedItem);
        }
    }
}
