﻿namespace M2L_JPO
{
    partial class inscriptionM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxLigue = new System.Windows.Forms.ComboBox();
            this.Membre = new System.Windows.Forms.Label();
            this.cbxMembre = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.cbxCrenaux = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(153, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(337, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = "Inscription des participants";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(115, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Ligue :";
            // 
            // cbxLigue
            // 
            this.cbxLigue.FormattingEnabled = true;
            this.cbxLigue.Location = new System.Drawing.Point(284, 110);
            this.cbxLigue.Name = "cbxLigue";
            this.cbxLigue.Size = new System.Drawing.Size(121, 21);
            this.cbxLigue.TabIndex = 12;
            // 
            // Membre
            // 
            this.Membre.AutoSize = true;
            this.Membre.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Membre.Location = new System.Drawing.Point(115, 156);
            this.Membre.Name = "Membre";
            this.Membre.Size = new System.Drawing.Size(71, 18);
            this.Membre.TabIndex = 13;
            this.Membre.Text = "Membre :";
            // 
            // cbxMembre
            // 
            this.cbxMembre.FormattingEnabled = true;
            this.cbxMembre.Location = new System.Drawing.Point(284, 157);
            this.cbxMembre.Name = "cbxMembre";
            this.cbxMembre.Size = new System.Drawing.Size(121, 21);
            this.cbxMembre.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(115, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 18);
            this.label7.TabIndex = 18;
            this.label7.Text = "Crénaux :";
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnregistrer.Location = new System.Drawing.Point(221, 304);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(84, 35);
            this.btnEnregistrer.TabIndex = 23;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // cbxCrenaux
            // 
            this.cbxCrenaux.FormattingEnabled = true;
            this.cbxCrenaux.Location = new System.Drawing.Point(284, 202);
            this.cbxCrenaux.Name = "cbxCrenaux";
            this.cbxCrenaux.Size = new System.Drawing.Size(121, 21);
            this.cbxCrenaux.TabIndex = 24;
            // 
            // inscriptionM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 407);
            this.Controls.Add(this.cbxCrenaux);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbxMembre);
            this.Controls.Add(this.Membre);
            this.Controls.Add(this.cbxLigue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "inscriptionM";
            this.Text = "inscriptionM";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxLigue;
        private System.Windows.Forms.Label Membre;
        private System.Windows.Forms.ComboBox cbxMembre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.ComboBox cbxCrenaux;
    }
}