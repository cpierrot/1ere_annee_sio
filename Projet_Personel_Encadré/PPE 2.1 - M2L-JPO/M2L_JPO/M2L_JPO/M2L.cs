﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;


namespace M2L_JPO
{
    public partial class M2L : Form
    {

        public M2L()
        {
            InitializeComponent();
        }

        private void M2L_Load(object sender, EventArgs e)
        {

            /*OleDbConnection uneConnexion = new OleDbConnection();
            String connexString = "Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data source = ..\..\..\";*/
        }

        

        private void inscriptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "inscription")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                inscription Finscription = new inscription();
                Finscription.MdiParent = this;
                Finscription.WindowState = FormWindowState.Maximized;
                Finscription.Show();

            }
        }

        private void participantsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "participants")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                participants Fparticipants = new participants();
                Fparticipants.MdiParent = this;
                Fparticipants.WindowState = FormWindowState.Maximized;
                Fparticipants.Show();

            }
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void liguesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "ligues")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                ligues Fligues = new ligues();
                Fligues.MdiParent = this;
                Fligues.WindowState = FormWindowState.Maximized;
                Fligues.Show();
            }
        }

        private void membresToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "membres")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                membres Fmembres = new membres();
                Fmembres.MdiParent = this;
                Fmembres.WindowState = FormWindowState.Maximized;
                Fmembres.Show();

            }
        }

        private void inscriptionMembresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "inscriptionM")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                membres Fmembres = new membres();
                Fmembres.MdiParent = this;
                Fmembres.WindowState = FormWindowState.Maximized;
                Fmembres.Show();
            }
        }
    }
}
    
