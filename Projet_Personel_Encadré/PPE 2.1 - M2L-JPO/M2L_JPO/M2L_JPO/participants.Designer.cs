﻿namespace M2L_JPO
{
    partial class participants
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbxParticipants = new System.Windows.Forms.ListBox();
            this.btnModifier = new System.Windows.Forms.Button();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.cbxCrenaux = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLongueurMod = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblLargeurMod = new System.Windows.Forms.Label();
            this.lbxLigue = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.btnModifierL = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(223, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Participants JPO";
            // 
            // lbxParticipants
            // 
            this.lbxParticipants.FormattingEnabled = true;
            this.lbxParticipants.Location = new System.Drawing.Point(12, 243);
            this.lbxParticipants.Name = "lbxParticipants";
            this.lbxParticipants.Size = new System.Drawing.Size(175, 173);
            this.lbxParticipants.TabIndex = 35;
            // 
            // btnModifier
            // 
            this.btnModifier.Location = new System.Drawing.Point(397, 329);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(77, 26);
            this.btnModifier.TabIndex = 36;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseVisualStyleBackColor = true;
            this.btnModifier.Click += new System.EventHandler(this.btnModifier_Click);
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.Location = new System.Drawing.Point(196, 378);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(77, 26);
            this.btnSupprimer.TabIndex = 37;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(193, 332);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 18);
            this.label7.TabIndex = 39;
            this.label7.Text = "Crénaux :";
            // 
            // cbxCrenaux
            // 
            this.cbxCrenaux.FormattingEnabled = true;
            this.cbxCrenaux.Location = new System.Drawing.Point(270, 333);
            this.cbxCrenaux.Name = "cbxCrenaux";
            this.cbxCrenaux.Size = new System.Drawing.Size(121, 21);
            this.cbxCrenaux.TabIndex = 40;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(163, 18);
            this.label2.TabIndex = 48;
            this.label2.Text = "Liste des participants";
            // 
            // lblLongueurMod
            // 
            this.lblLongueurMod.AutoSize = true;
            this.lblLongueurMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLongueurMod.Location = new System.Drawing.Point(342, 222);
            this.lblLongueurMod.Name = "lblLongueurMod";
            this.lblLongueurMod.Size = new System.Drawing.Size(73, 18);
            this.lblLongueurMod.TabIndex = 49;
            this.lblLongueurMod.Text = "longueur :";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(572, 220);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(70, 18);
            this.textBox1.TabIndex = 51;
            // 
            // lblLargeurMod
            // 
            this.lblLargeurMod.AutoSize = true;
            this.lblLargeurMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLargeurMod.Location = new System.Drawing.Point(497, 220);
            this.lblLargeurMod.Name = "lblLargeurMod";
            this.lblLargeurMod.Size = new System.Drawing.Size(69, 18);
            this.lblLargeurMod.TabIndex = 53;
            this.lblLargeurMod.Text = "largueur :";
            // 
            // lbxLigue
            // 
            this.lbxLigue.FormattingEnabled = true;
            this.lbxLigue.Location = new System.Drawing.Point(12, 46);
            this.lbxLigue.Name = "lbxLigue";
            this.lbxLigue.Size = new System.Drawing.Size(175, 173);
            this.lbxLigue.TabIndex = 54;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 18);
            this.label3.TabIndex = 55;
            this.label3.Text = "Liste des ligue";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(338, 92);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(77, 26);
            this.button2.TabIndex = 56;
            this.button2.Text = "Ajouter";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(542, 366);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 50);
            this.button1.TabIndex = 57;
            this.button1.Text = "Annuler";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(193, 193);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(77, 26);
            this.button3.TabIndex = 58;
            this.button3.Text = "Supprimer";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(229, 92);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(100, 95);
            this.listBox1.TabIndex = 59;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(421, 92);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(100, 95);
            this.listBox2.TabIndex = 60;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(421, 222);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(70, 18);
            this.textBox2.TabIndex = 61;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(338, 161);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(77, 26);
            this.button4.TabIndex = 62;
            this.button4.Text = "Retirer";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnModifierL
            // 
            this.btnModifierL.Location = new System.Drawing.Point(541, 126);
            this.btnModifierL.Name = "btnModifierL";
            this.btnModifierL.Size = new System.Drawing.Size(77, 26);
            this.btnModifierL.TabIndex = 63;
            this.btnModifierL.Text = "Modifier";
            this.btnModifierL.UseVisualStyleBackColor = true;
            // 
            // participants
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 428);
            this.Controls.Add(this.btnModifierL);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbxLigue);
            this.Controls.Add(this.lblLargeurMod);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblLongueurMod);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbxCrenaux);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnSupprimer);
            this.Controls.Add(this.btnModifier);
            this.Controls.Add(this.lbxParticipants);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "participants";
            this.Text = "participants";
            this.Load += new System.EventHandler(this.participants_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbxParticipants;
        private System.Windows.Forms.Button btnModifier;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbxCrenaux;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLongueurMod;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblLargeurMod;
        private System.Windows.Forms.ListBox lbxLigue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnModifierL;
    }
}