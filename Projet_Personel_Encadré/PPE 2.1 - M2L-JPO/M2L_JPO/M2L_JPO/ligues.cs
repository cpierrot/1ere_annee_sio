﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;


namespace M2L_JPO
{
    public partial class ligues : Form
    {
        public ligues()
        {
            InitializeComponent();

        }

        private void Ligue_Load(object sender, EventArgs e)
        {
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data source=..\..\..\Base_de_données M2L-JPO.accdb");

            OleDbCommand cmd = new OleDbCommand("SELECT * FROM LIGUE", cnn);

            OleDbDataReader dr;

            try
            {
                cnn.Open();
                dr = cmd.ExecuteReader
            (CommandBehavior.CloseConnection);

                while (dr.Read())
                {
                    lbxLigues.Items.Add(dr["nom"]);
                }
            }
            catch { }
            finally
            {
                cnn.Close();
            }




        }

        public void actualiser()
        {
            lbxLigues.Items.Clear();
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data source=..\..\..\Base_de_données M2L-JPO.accdb");

            OleDbCommand cmd = new OleDbCommand("SELECT * FROM LIGUE", cnn);

            OleDbDataReader dr;

            try
            {
                cnn.Open();
                dr = cmd.ExecuteReader
            (CommandBehavior.CloseConnection);

                while (dr.Read())
                {
                    lbxLigues.Items.Add(dr["nom"]);
                }
            }
            catch { }
            finally
            {
                cnn.Close();
            }
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data source=..\..\..\Base_de_données M2L-JPO.accdb");
            OleDbDataReader dr;
            OleDbCommand cmd = new OleDbCommand("UPDATE LIGUE " +
                "SET nom ='" + tbxLigue.Text + "'," +
                " adresse = '" + tbxAdresse.Text + "'," +
                " discipline = '" + tbxDiscipline.Text + "' " +
                "WHERE nom = '"+lbxLigues.SelectedItem+"'; ", cnn);
            try
            {
                cnn.Open();
                dr = cmd.ExecuteReader
            (CommandBehavior.CloseConnection);

            }
            catch { }
            finally
            {
                cnn.Close();
            }

            actualiser();
            
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data source=..\..\..\Base_de_données M2L-JPO.accdb");
            OleDbDataReader dr;
            OleDbCommand cmd = new OleDbCommand("insert into LIGUE (nom, adresse, discipline) values( '" + tbxLigue.Text + "',  '" + tbxAdresse.Text + "',  '" + tbxDiscipline.Text + "'); ", cnn);
            try
            {
                cnn.Open();
                dr = cmd.ExecuteReader
            (CommandBehavior.CloseConnection);

            }
            catch { }
            finally
            {
                cnn.Close();
            }

            actualiser();

            tbxDiscipline.Text = null;
            tbxAdresse.Text = null;
            tbxLigue.Text = null;
        }

        private void lbxLigues_SelectedIndexChanged(object sender, EventArgs e)
        {
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data source=..\..\..\Base_de_données M2L-JPO.accdb");
            OleDbDataReader dr;
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM LIGUE WHERE nom = '"+ lbxLigues.SelectedItem +"';", cnn);
            try
            {
                cnn.Open();

                dr = cmd.ExecuteReader
            (CommandBehavior.CloseConnection);

                while (dr.Read())
                {
                    tbxLigue.Text = Convert.ToString(dr["nom"]);
                    tbxAdresse.Text = Convert.ToString(dr["adresse"]);
                    tbxDiscipline.Text = Convert.ToString(dr["discipline"]);
                }
            }
            catch { }
            finally
            {
                cnn.Close();
            }
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            OleDbConnection cnn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" + @"Data source=..\..\..\Base_de_données M2L-JPO.accdb");
            OleDbDataReader dr;
            OleDbCommand cmd = new OleDbCommand("DELETE FROM LIGUE WHERE nom = '" + lbxLigues.SelectedItem + "';", cnn);
            try
            {
                cnn.Open();
                dr = cmd.ExecuteReader
            (CommandBehavior.CloseConnection);
            }
            catch { }
            finally
            {
                cnn.Close();
            }

            actualiser();
        }
    }
}
