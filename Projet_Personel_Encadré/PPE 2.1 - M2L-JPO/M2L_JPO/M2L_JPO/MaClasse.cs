﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2L_JPO
{
    public class MaClasse
    {
        private int iValeur = 0;
        private string sNom = "";

        public MaClasse(int iValeur, string sNom)
        {
            this.iValeur = iValeur;
            this.sNom = sNom;
        }

        public int getValeur()
        {
            return (this.iValeur);
        }

        public string getNom()
        {
            return (this.sNom);
        }

        public override string ToString()
        {
            return (this.getNom());
        }
    }
}
