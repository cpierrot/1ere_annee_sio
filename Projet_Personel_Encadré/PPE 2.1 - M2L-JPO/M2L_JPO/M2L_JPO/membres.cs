﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace M2L_JPO
{
    public partial class membres : Form
    {
        public membres()
        {
            InitializeComponent();
        }

        private void membres_Load(object sender, EventArgs e)
        {
            //tbxLigue.Enabled = false;
            tbxMail.Enabled = false;
            tbxNom.Enabled = false;
            tbxPrenom.Enabled = false;
            tbxTelephone.Enabled = false;
            btnEnregistrer.Enabled = false;
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            //tbxLigue.Enabled = true;
            tbxMail.Enabled = true;
            tbxNom.Enabled = true;
            tbxPrenom.Enabled = true;
            tbxTelephone.Enabled = true;
            btnEnregistrer.Enabled = true;
        }

        private void btnNouveau_Click(object sender, EventArgs e)
        {
            //tbxLigue.Enabled = true;
            tbxMail.Enabled = true;
            tbxNom.Enabled = true;
            tbxPrenom.Enabled = true;
            tbxTelephone.Enabled = true;
            btnEnregistrer.Enabled = true;
        }
    }
}
