﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class participants : Form
    {
        public participants()
        {
            InitializeComponent();
        }

        private void participants_Load(object sender, EventArgs e)
        {
            btnEnregistrer.Enabled = false;
            cbxCrenaux.Enabled = false;
            tbxLargeur.Enabled = false;
            tbxLongueur.Enabled = false;
            cblEquipement.Enabled = false;
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            btnEnregistrer.Enabled = true;
            cbxCrenaux.Enabled = true;
            tbxLargeur.Enabled = true;
            tbxLongueur.Enabled = true;
            cblEquipement.Enabled = true;

        }
    }
}
