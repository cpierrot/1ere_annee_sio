﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class M2LReservationSalles : Form
    {

        public static DataSet reservationsSallesDataSet;
        public M2LReservationSalles()
        {
            InitializeComponent();
        }


        private void M2LReservationSalles_Load(object sender, EventArgs e)
        {
            dbConnexion.setDataSet();
            reservationsSallesDataSet = dbConnexion.getDataSet();
        }


        private void sallesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "Ligues")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                ligues Fligues = new ligues();
                Fligues.MdiParent = this;
                Fligues.WindowState = FormWindowState.Maximized;
                Fligues.Show();
            }

        }


      


     

        private void réservantsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "Membres")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                membres Freservants = new membres();
                Freservants.MdiParent = this;
                Freservants.WindowState = FormWindowState.Maximized;
                Freservants.Show();

            }

        }

        private void réservationsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "Inscription")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                inscription Finscription = new inscription();
                Finscription.MdiParent = this;
                Finscription.WindowState = FormWindowState.Maximized;
                Finscription.Show();

            }
        }

        private void quitterToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
