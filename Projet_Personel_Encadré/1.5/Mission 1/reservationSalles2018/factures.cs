﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class factures : Form
    {
        DataTable tableSalles = M2LReservationSalles.reservationsSallesDataSet.Tables["Salles"];
        DataTable tableReservants = M2LReservationSalles.reservationsSallesDataSet.Tables["Reservants"];
        DataTable tableReservations = M2LReservationSalles.reservationsSallesDataSet.Tables["Reservations"];

        DataRelation reservantsReservations = M2LReservationSalles.reservationsSallesDataSet.Relations["EquiJoin"];
        DataRelation sallessReservations = M2LReservationSalles.reservationsSallesDataSet.Relations["EquiJoin2"];
        String[,] tabReservations;

        public factures()
        {
            InitializeComponent();
        }

        private void factures_Load(object sender, EventArgs e)
        {
            int nbLignesReservations, nbColonnesReservations = 5, i;
            int k = 0, j;
            
           

            nbLignesReservations = reservantsReservations.ChildTable.Rows.Count;
            tabReservations = new String[nbLignesReservations, nbColonnesReservations];

           

            i = 0;
            foreach (DataRow reservationsLigne in reservantsReservations.ChildTable.Rows)
            {
                tabReservations[i, 1] = reservationsLigne.GetParentRow(reservantsReservations)["nomReservant"].ToString();
                tabReservations[i, 0] = Convert.ToDateTime(reservationsLigne["dateReservation"]).ToString("MMMM yyyy");
                tabReservations[i, 2] = reservationsLigne.GetParentRow(sallessReservations)["typeSalle"].ToString();
                tabReservations[i, 3] = reservationsLigne["plageReservation"].ToString();
                tabReservations[i, 4] = reservationsLigne.GetParentRow(sallessReservations)["prixLocationSalle"].ToString();
                i++;
            }

           


            const byte NB_COL = 5;
          

            dgvFactures.ColumnCount = NB_COL;

            dgvFactures.ColumnHeadersVisible = true;
            dgvFactures.Columns[0].HeaderText = "Date";
            dgvFactures.Columns[1].HeaderText = "Réservant";
            dgvFactures.Columns[2].HeaderText = "Type de Salle";
            dgvFactures.Columns[3].HeaderText = "Plage";
            dgvFactures.Columns[4].HeaderText = "Prix";



            dgvFactures.ReadOnly = true;
            dgvFactures.RowHeadersVisible = false;

            for (i = 0; i < dgvFactures.ColumnCount; i++)
            {
                dgvFactures.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            }

            
            //Liste déroulante des Ligues
            cbxLigue.DataSource = tableReservants;
            cbxLigue.DisplayMember = tableReservants.Columns[1].ToString();
            cbxLigue.ValueMember = tableReservants.Columns[1].ToString();

            //Mois et Année            
            i = 0;
            string[] ordre = new string[tabReservations.GetLength(0)];
            while(i< tabReservations.GetLength(0) && tabReservations[i, 0] != null)
            {
                j = 0;
                while(j<k && tabReservations[i, 0] != ordre[j])
                {
                    j++;
                }
                if (tabReservations[i,0]!= ordre[j])
                {
                    ordre[j] = tabReservations[i, 0];
                    k++;
                }
                i++;
            }

            for ( i = 0; i < ordre.Length; i++)
            {
                cbxMois.Items.Add(ordre[i]);
            }
            cbxMois.SelectedIndex = 0;
            
            cbxLigue.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxMois.DropDownStyle = ComboBoxStyle.DropDownList;

        }

        private void dgvFactures_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnEffacer_Click(object sender, EventArgs e)
        {
            lblNbReserv.Text = null;
            lblTotal.Text = null;
            cbxLigue.Text = null;
            cbxMois.Text = null;
            dgvFactures.Rows.Clear();
        }

        private void lblTotal_Click(object sender, EventArgs e)
        {
            
        }

        private void btnValider_Click(object sender, EventArgs e)
        {
            double prix = 0;
            int i,j;
            DateTime date = Convert.ToDateTime(cbxMois.SelectedValue);
            int NBLIGNES = tabReservations.GetLength(0);
            int NBCOLONNES = tabReservations.GetLength(1);
            dgvFactures.Rows.Clear();
            
            //Afficher les réservations par mois
            i = 0;
            j = 0;
            while (i < NBLIGNES)
            {
               
                
                //Trié par ligue
                
                  if ((Convert.ToString(cbxMois.SelectedItem) == tabReservations[i, 0]) && (Convert.ToString(cbxLigue.SelectedValue) == tabReservations[i, 1]))
                  {
                        
                      dgvFactures.Rows.Add();
                      dgvFactures[1, j].Value = tabReservations[i, 1];
                      dgvFactures[0, j].Value = tabReservations[i, 0];
                      dgvFactures[2, j].Value = tabReservations[i, 2];
                      dgvFactures[3, j].Value = tabReservations[i, 3];
                      dgvFactures[4, j].Value = tabReservations[i, 4];
                      j++;
                  }
                   i++;
                  
            }

            //Montant total des réservations
            for (i = 0; i < dgvFactures.Rows.Count; i++)
            {
                prix = prix + Convert.ToDouble(dgvFactures[4, i].Value);
            }
            lblTotal.Text = Convert.ToString(prix) + " €";

            //Nombre total des réservations
            lblNbReserv.Text = Convert.ToString(dgvFactures.Rows.Count);
            

        }

        private void btnTout_Click(object sender, EventArgs e)
        {
            int i=0, j=0;
            double prix = 0;
            int NBLIGNES = tabReservations.GetLength(0);
            dgvFactures.Rows.Clear();
            while (i < NBLIGNES)
            {
                //Trié par Mois
                if (Convert.ToString(cbxMois.SelectedItem) == tabReservations[i, 0])
                {

                    dgvFactures.Rows.Add();
                    dgvFactures[1, j].Value = tabReservations[i, 1];
                    dgvFactures[0, j].Value = tabReservations[i, 0];
                    dgvFactures[2, j].Value = tabReservations[i, 2];
                    dgvFactures[3, j].Value = tabReservations[i, 3];
                    dgvFactures[4, j].Value = tabReservations[i, 4];
                    j++;
                }
                i++;
            }
            //Montant total des réservations
            for (i = 0; i < dgvFactures.Rows.Count; i++)
            {
                prix = prix + Convert.ToDouble(dgvFactures[4, i].Value);
            }
            lblTotal.Text = Convert.ToString(prix) + " €";

            //Nombre total des réservations
            lblNbReserv.Text = Convert.ToString(dgvFactures.Rows.Count);

        }
    }
}
