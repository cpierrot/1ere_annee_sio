﻿namespace reservationSalles2018
{
    partial class factures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvFactures = new System.Windows.Forms.DataGridView();
            this.txtDate = new System.Windows.Forms.Label();
            this.txtLigue = new System.Windows.Forms.Label();
            this.cbxLigue = new System.Windows.Forms.ComboBox();
            this.btnValider = new System.Windows.Forms.Button();
            this.btnEffacer = new System.Windows.Forms.Button();
            this.lblNbReserv = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.textNb = new System.Windows.Forms.Label();
            this.textMontant = new System.Windows.Forms.Label();
            this.cbxMois = new System.Windows.Forms.ComboBox();
            this.btnTout = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactures)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFactures
            // 
            this.dgvFactures.AllowUserToAddRows = false;
            this.dgvFactures.AllowUserToDeleteRows = false;
            this.dgvFactures.AllowUserToResizeColumns = false;
            this.dgvFactures.AllowUserToResizeRows = false;
            this.dgvFactures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFactures.Location = new System.Drawing.Point(12, 29);
            this.dgvFactures.MultiSelect = false;
            this.dgvFactures.Name = "dgvFactures";
            this.dgvFactures.Size = new System.Drawing.Size(520, 377);
            this.dgvFactures.TabIndex = 0;
            this.dgvFactures.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFactures_CellContentClick);
            // 
            // txtDate
            // 
            this.txtDate.AutoSize = true;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(593, 52);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(47, 18);
            this.txtDate.TabIndex = 1;
            this.txtDate.Text = "Date :";
            // 
            // txtLigue
            // 
            this.txtLigue.AutoSize = true;
            this.txtLigue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLigue.Location = new System.Drawing.Point(593, 122);
            this.txtLigue.Name = "txtLigue";
            this.txtLigue.Size = new System.Drawing.Size(51, 18);
            this.txtLigue.TabIndex = 3;
            this.txtLigue.Text = "Ligue :";
            // 
            // cbxLigue
            // 
            this.cbxLigue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxLigue.FormattingEnabled = true;
            this.cbxLigue.Location = new System.Drawing.Point(773, 123);
            this.cbxLigue.Name = "cbxLigue";
            this.cbxLigue.Size = new System.Drawing.Size(142, 21);
            this.cbxLigue.TabIndex = 4;
            this.cbxLigue.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // btnValider
            // 
            this.btnValider.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValider.Location = new System.Drawing.Point(651, 387);
            this.btnValider.Name = "btnValider";
            this.btnValider.Size = new System.Drawing.Size(109, 51);
            this.btnValider.TabIndex = 5;
            this.btnValider.Text = "Valider";
            this.btnValider.UseVisualStyleBackColor = true;
            this.btnValider.Click += new System.EventHandler(this.btnValider_Click);
            // 
            // btnEffacer
            // 
            this.btnEffacer.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEffacer.Location = new System.Drawing.Point(874, 387);
            this.btnEffacer.Name = "btnEffacer";
            this.btnEffacer.Size = new System.Drawing.Size(104, 51);
            this.btnEffacer.TabIndex = 6;
            this.btnEffacer.Text = "Réinitialiser";
            this.btnEffacer.UseVisualStyleBackColor = true;
            this.btnEffacer.Click += new System.EventHandler(this.btnEffacer_Click);
            // 
            // lblNbReserv
            // 
            this.lblNbReserv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNbReserv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbReserv.Location = new System.Drawing.Point(773, 203);
            this.lblNbReserv.Name = "lblNbReserv";
            this.lblNbReserv.Size = new System.Drawing.Size(142, 28);
            this.lblNbReserv.TabIndex = 7;
            this.lblNbReserv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTotal
            // 
            this.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(773, 288);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(142, 29);
            this.lblTotal.TabIndex = 8;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTotal.Click += new System.EventHandler(this.lblTotal_Click);
            // 
            // textNb
            // 
            this.textNb.AutoSize = true;
            this.textNb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNb.Location = new System.Drawing.Point(593, 213);
            this.textNb.Name = "textNb";
            this.textNb.Size = new System.Drawing.Size(167, 18);
            this.textNb.TabIndex = 9;
            this.textNb.Text = "Nombre de réservation :";
            // 
            // textMontant
            // 
            this.textMontant.AutoSize = true;
            this.textMontant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMontant.Location = new System.Drawing.Point(593, 299);
            this.textMontant.Name = "textMontant";
            this.textMontant.Size = new System.Drawing.Size(102, 18);
            this.textMontant.TabIndex = 10;
            this.textMontant.Text = "Montant total :";
            // 
            // cbxMois
            // 
            this.cbxMois.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMois.FormattingEnabled = true;
            this.cbxMois.Location = new System.Drawing.Point(773, 53);
            this.cbxMois.Name = "cbxMois";
            this.cbxMois.Size = new System.Drawing.Size(142, 21);
            this.cbxMois.TabIndex = 11;
            // 
            // btnTout
            // 
            this.btnTout.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTout.ForeColor = System.Drawing.Color.Black;
            this.btnTout.Location = new System.Drawing.Point(950, 109);
            this.btnTout.Name = "btnTout";
            this.btnTout.Size = new System.Drawing.Size(75, 47);
            this.btnTout.TabIndex = 12;
            this.btnTout.Text = "Toutes les ligues";
            this.btnTout.UseVisualStyleBackColor = true;
            this.btnTout.Click += new System.EventHandler(this.btnTout_Click);
            // 
            // factures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 463);
            this.Controls.Add(this.btnTout);
            this.Controls.Add(this.cbxMois);
            this.Controls.Add(this.textMontant);
            this.Controls.Add(this.textNb);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblNbReserv);
            this.Controls.Add(this.btnEffacer);
            this.Controls.Add(this.btnValider);
            this.Controls.Add(this.cbxLigue);
            this.Controls.Add(this.txtLigue);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.dgvFactures);
            this.Name = "factures";
            this.Text = "factures";
            this.Load += new System.EventHandler(this.factures_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactures)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFactures;
        private System.Windows.Forms.Label txtDate;
        private System.Windows.Forms.Label txtLigue;
        private System.Windows.Forms.ComboBox cbxLigue;
        private System.Windows.Forms.Button btnValider;
        private System.Windows.Forms.Button btnEffacer;
        private System.Windows.Forms.Label lblNbReserv;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label textNb;
        private System.Windows.Forms.Label textMontant;
        private System.Windows.Forms.ComboBox cbxMois;
        private System.Windows.Forms.Button btnTout;
    }
}