﻿namespace GSB
{
    partial class frmComptaListeFiches
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmComptaListeFiches));
            this.dgvFiches = new System.Windows.Forms.DataGridView();
            this.btnOuvrirFiche = new System.Windows.Forms.Button();
            this.pbxLogo = new System.Windows.Forms.PictureBox();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.VisiteurGestionnaire = new System.Windows.Forms.TextBox();
            this.lbMois = new System.Windows.Forms.Label();
            this.lbAnnee = new System.Windows.Forms.Label();
            this.lbEtat = new System.Windows.Forms.Label();
            this.lbVisiteur = new System.Windows.Forms.Label();
            this.cbxMois = new System.Windows.Forms.ComboBox();
            this.cbxAnnee = new System.Windows.Forms.ComboBox();
            this.cbxEtat = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnRechercher = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFiches
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFiches.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvFiches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFiches.Location = new System.Drawing.Point(12, 12);
            this.dgvFiches.Name = "dgvFiches";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvFiches.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvFiches.Size = new System.Drawing.Size(680, 292);
            this.dgvFiches.TabIndex = 0;
            this.dgvFiches.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFiches_CellContentClick);
            // 
            // btnOuvrirFiche
            // 
            this.btnOuvrirFiche.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOuvrirFiche.Location = new System.Drawing.Point(715, 223);
            this.btnOuvrirFiche.Name = "btnOuvrirFiche";
            this.btnOuvrirFiche.Size = new System.Drawing.Size(237, 38);
            this.btnOuvrirFiche.TabIndex = 1;
            this.btnOuvrirFiche.Text = "Ouvrir fiche sélectionnée";
            this.btnOuvrirFiche.UseVisualStyleBackColor = true;
            this.btnOuvrirFiche.Click += new System.EventHandler(this.btnOuvrirFiche_Click);
            // 
            // pbxLogo
            // 
            this.pbxLogo.BackColor = System.Drawing.Color.Transparent;
            this.pbxLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxLogo.BackgroundImage")));
            this.pbxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxLogo.Location = new System.Drawing.Point(715, 32);
            this.pbxLogo.Name = "pbxLogo";
            this.pbxLogo.Size = new System.Drawing.Size(237, 139);
            this.pbxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxLogo.TabIndex = 10;
            this.pbxLogo.TabStop = false;
            // 
            // btnQuitter
            // 
            this.btnQuitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitter.Location = new System.Drawing.Point(715, 306);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(237, 38);
            this.btnQuitter.TabIndex = 11;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // VisiteurGestionnaire
            // 
            this.VisiteurGestionnaire.Location = new System.Drawing.Point(80, 405);
            this.VisiteurGestionnaire.Name = "VisiteurGestionnaire";
            this.VisiteurGestionnaire.Size = new System.Drawing.Size(111, 20);
            this.VisiteurGestionnaire.TabIndex = 13;
            // 
            // lbMois
            // 
            this.lbMois.AutoSize = true;
            this.lbMois.Location = new System.Drawing.Point(20, 314);
            this.lbMois.Name = "lbMois";
            this.lbMois.Size = new System.Drawing.Size(32, 13);
            this.lbMois.TabIndex = 16;
            this.lbMois.Text = "Mois:";
            this.lbMois.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbAnnee
            // 
            this.lbAnnee.AutoSize = true;
            this.lbAnnee.Location = new System.Drawing.Point(17, 347);
            this.lbAnnee.Name = "lbAnnee";
            this.lbAnnee.Size = new System.Drawing.Size(41, 13);
            this.lbAnnee.TabIndex = 17;
            this.lbAnnee.Text = "Annee:";
            // 
            // lbEtat
            // 
            this.lbEtat.AutoSize = true;
            this.lbEtat.Location = new System.Drawing.Point(29, 379);
            this.lbEtat.Name = "lbEtat";
            this.lbEtat.Size = new System.Drawing.Size(29, 13);
            this.lbEtat.TabIndex = 18;
            this.lbEtat.Text = "Etat:";
            // 
            // lbVisiteur
            // 
            this.lbVisiteur.AutoSize = true;
            this.lbVisiteur.Location = new System.Drawing.Point(9, 408);
            this.lbVisiteur.Name = "lbVisiteur";
            this.lbVisiteur.Size = new System.Drawing.Size(69, 13);
            this.lbVisiteur.TabIndex = 19;
            this.lbVisiteur.Text = "Nom Visiteur:";
            this.lbVisiteur.Click += new System.EventHandler(this.lbVisiteur_Click);
            // 
            // cbxMois
            // 
            this.cbxMois.FormattingEnabled = true;
            this.cbxMois.Location = new System.Drawing.Point(80, 310);
            this.cbxMois.Name = "cbxMois";
            this.cbxMois.Size = new System.Drawing.Size(111, 21);
            this.cbxMois.TabIndex = 21;
            this.cbxMois.SelectedIndexChanged += new System.EventHandler(this.cbxMois_SelectedIndexChanged);
            // 
            // cbxAnnee
            // 
            this.cbxAnnee.FormattingEnabled = true;
            this.cbxAnnee.Location = new System.Drawing.Point(80, 344);
            this.cbxAnnee.Name = "cbxAnnee";
            this.cbxAnnee.Size = new System.Drawing.Size(111, 21);
            this.cbxAnnee.TabIndex = 22;
            this.cbxAnnee.SelectedIndexChanged += new System.EventHandler(this.cbxAnnee_SelectedIndexChanged);
            // 
            // cbxEtat
            // 
            this.cbxEtat.FormattingEnabled = true;
            this.cbxEtat.Location = new System.Drawing.Point(80, 376);
            this.cbxEtat.Name = "cbxEtat";
            this.cbxEtat.Size = new System.Drawing.Size(111, 21);
            this.cbxEtat.TabIndex = 23;
            this.cbxEtat.SelectedIndexChanged += new System.EventHandler(this.cbxEtat_SelectedIndexChanged);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(455, 322);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(237, 38);
            this.button3.TabIndex = 29;
            this.button3.Text = "Supprimer";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(455, 366);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(237, 38);
            this.button1.TabIndex = 30;
            this.button1.Text = "Passer à l\'étape supérieure";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(354, 371);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(78, 31);
            this.btnRefresh.TabIndex = 31;
            this.btnRefresh.Text = "Rafraichir";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnRechercher
            // 
            this.btnRechercher.Location = new System.Drawing.Point(227, 371);
            this.btnRechercher.Name = "btnRechercher";
            this.btnRechercher.Size = new System.Drawing.Size(98, 31);
            this.btnRechercher.TabIndex = 32;
            this.btnRechercher.Text = "Rechercher";
            this.btnRechercher.UseVisualStyleBackColor = true;
            this.btnRechercher.Click += new System.EventHandler(this.btnRechercher_Click);
            // 
            // frmComptaListeFiches
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(967, 452);
            this.Controls.Add(this.btnRechercher);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.cbxEtat);
            this.Controls.Add(this.cbxAnnee);
            this.Controls.Add(this.cbxMois);
            this.Controls.Add(this.lbVisiteur);
            this.Controls.Add(this.lbEtat);
            this.Controls.Add(this.lbAnnee);
            this.Controls.Add(this.lbMois);
            this.Controls.Add(this.VisiteurGestionnaire);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.pbxLogo);
            this.Controls.Add(this.btnOuvrirFiche);
            this.Controls.Add(this.dgvFiches);
            this.Name = "frmComptaListeFiches";
            this.Text = "GSB Gestion des frais - Compta Fiches de frais";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmComptaListeFiches_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFiches;
        private System.Windows.Forms.Button btnOuvrirFiche;
        internal System.Windows.Forms.PictureBox pbxLogo;
        private System.Windows.Forms.Button btnQuitter;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox VisiteurGestionnaire;
        private System.Windows.Forms.Label lbMois;
        private System.Windows.Forms.Label lbAnnee;
        private System.Windows.Forms.Label lbEtat;
        private System.Windows.Forms.Label lbVisiteur;
        private System.Windows.Forms.ComboBox cbxMois;
        private System.Windows.Forms.ComboBox cbxAnnee;
        private System.Windows.Forms.ComboBox cbxEtat;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnRechercher;
    }
}