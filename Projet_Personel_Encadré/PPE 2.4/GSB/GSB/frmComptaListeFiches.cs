﻿using dao;
using metier;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB
{
    public partial class frmComptaListeFiches : Form
    {
        private int idFicheFraisSelectionne;
        public frmComptaListeFiches()
        {

            InitializeComponent();

           /********************************************************************************
            * Mise en forme du datagridview (liste des fiches de frais)
            * ******************************************************************************/
            dgvFiches.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvFiches.MultiSelect = true;
            dgvFiches.RowHeadersVisible = false;
            dgvFiches.ColumnHeadersVisible = true;
            dgvFiches.AllowUserToAddRows = false;
            dgvFiches.ReadOnly = true;
            dgvFiches.ScrollBars = ScrollBars.Vertical;
            dgvFiches.ColumnCount = 4;
            dgvFiches.AllowUserToResizeRows = false;


            dgvFiches.Columns[0].Width = 80;
            dgvFiches.Columns[1].Width = 230;
            dgvFiches.Columns[2].Width = 170;
            dgvFiches.Columns[3].Width = 200;
            dgvFiches.Width = 680;

            dgvFiches.Columns[0].HeaderText = "Code";
            dgvFiches.Columns[1].HeaderText = "Visiteur";
            dgvFiches.Columns[2].HeaderText = "Mois";
            dgvFiches.Columns[3].HeaderText = "Etat";


            /********************************************************************************
           * Remplissage du datagridview (liste des fiches de frais) à partir de la classe LesFichesFrais
           * ******************************************************************************/
           

            remplirDgvFiches();
            remplircbx();
        }

        /* AJOUT CORALEE */
        public void remplircbx()
        {
            try
            {
                dgvFiches.RowCount = LesFichesFrais.nbFichesFrais();

                cbxAnnee.Items.Add("Tous");
                cbxMois.Items.Add("Tous");
                cbxEtat.Items.Add("Tous");
                
                foreach (FicheFrais uneFicheFrais in LesFichesFrais.obtenirLesFichesFrais())
                {
                    bool doublon = false;
            /**  REMPLIR LES MOIS  **/
                    String mois = uneFicheFrais.getMoisAnnee();
                    mois = mois.Substring(9, mois.Length - 10).ToString();

                    
                    for (int j = 0; j < cbxMois.Items.Count; j++)
                    {
                        if(cbxMois.Items[j].ToString()== mois)
                        {   
                            doublon = true;
                        }
                    }
                    if (doublon == false)
                    {
                        cbxMois.Items.Add(mois);
                    }

            /** REMPLIR LES ANNEES **/
                    doublon = false;
                    String annee = uneFicheFrais.getMoisAnnee();
                    annee = annee.Substring(0, 4).ToString();

                    for (int j = 0; j < cbxAnnee.Items.Count; j++)
                    {
                        if (cbxAnnee.Items[j].ToString() == annee)
                        {
                            doublon = true;
                        }
                    }
                    if (doublon == false)
                    {
                        cbxAnnee.Items.Add(annee);
                    }

            /** REMPLIR LES ETATS **/
                    doublon = false;
                    String etat = uneFicheFrais.getEtatLong();

                    for (int j = 0; j < cbxEtat.Items.Count; j++)
                    {
                        if (cbxEtat.Items[j].ToString() == etat)
                        {
                            doublon = true;
                        }
                    }
                    if (doublon == false)
                    {
                        cbxEtat.Items.Add(etat);
                    }
                }
                cbxMois.SelectedIndex = 0;
                cbxAnnee.SelectedIndex = 0;
                cbxEtat.SelectedIndex = 0;

            }
            catch
            {
                MessageBox.Show("Fiches de frais introuvales");
            }
            
        }

            public void remplirDgvFiches()
        {


            try
            {
                dgvFiches.RowCount = LesFichesFrais.nbFichesFrais();

                int ligne = 0;
                foreach (FicheFrais uneFicheFrais in LesFichesFrais.obtenirLesFichesFrais())
                {
                    dgvFiches[0, ligne].Value = uneFicheFrais.getId();
                    dgvFiches[1, ligne].Value = uneFicheFrais.getNomPrenomVisiteur();
                    dgvFiches[2, ligne].Value = uneFicheFrais.getMoisAnnee();
                    dgvFiches[3, ligne].Value = uneFicheFrais.getEtatLong();
                    ligne++;
                }



                dgvFiches.Sort(dgvFiches.Columns[2], System.ComponentModel.ListSortDirection.Descending);

               
                



            }
            catch
            {
                MessageBox.Show("Fiches de frais introuvales");
            }

        }


        /// <summary>
        /// Méthode permettant de récupérer l'id de la fiche de frais sélectionnée
        /// </summary>
        /// <returns></returns>
        public int getIdFicheFraisSelectionne()
        {
            return idFicheFraisSelectionne;
        }


        /// <summary>
        /// Ouvrir le formulaire pour une fiche de frais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOuvrirFiche_Click(object sender, EventArgs e)
        {
            if(dgvFiches.SelectedRows.Count == 0)
            {
                MessageBox.Show("Vous devez sélectionner une fiche de frais.");
            }
            else
            {
                idFicheFraisSelectionne = (int)dgvFiches.CurrentRow.Cells[0].Value;
                frmComptaFiche comptaFiche = new frmComptaFiche();
                comptaFiche.Owner = this;
                comptaFiche.ShowDialog();
            }
            
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            DbConnexion.SeDeconnecter();
            this.Close(); 
        }

        private void frmComptaListeFiches_FormClosed(object sender, FormClosedEventArgs e)
        {
            DbConnexion.SeDeconnecter();
        }

        private void dgvFiches_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void lbVisiteur_Click(object sender, EventArgs e)
        {
        }

        /* AJOUT CORALEE */
        private void cbxMois_SelectedIndexChanged(object sender, EventArgs e)
        { 
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void cbxAnnee_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            remplirDgvFiches();
            cbxMois.SelectedIndex = 0;
            cbxAnnee.SelectedIndex = 0;
            cbxEtat.SelectedIndex = 0;
        }

        private void btnRechercher_Click(object sender, EventArgs e)
        {
            
            remplirDgvFiches();
            dgvFiches.RowCount = LesFichesFrais.nbFichesFrais();
            int ligne = 0;
            foreach (FicheFrais uneFicheFrais in LesFichesFrais.obtenirLesFichesFrais())
            {
                
                string mois = dgvFiches[2, ligne].Value.ToString();
                mois = mois.Substring(9, dgvFiches[2, ligne].Value.ToString().Length - 10).ToString();
                string annee = dgvFiches[2, ligne].Value.ToString();
                annee = annee.Substring(0, 4).ToString();
                string etat = dgvFiches[3, ligne].Value.ToString();
                string nom = dgvFiches[1, ligne].Value.ToString();

                /***********  Effacer lignes vides : conditions ***********/
                if (cbxAnnee.SelectedIndex ==0 && cbxMois.SelectedIndex==0 && VisiteurGestionnaire.Text == "" && cbxEtat.SelectedIndex != 0)
                {
                    
                    if (cbxEtat.Text != etat)
                    {
                        
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if(cbxAnnee.SelectedIndex==0 && cbxEtat.SelectedIndex == 0 && VisiteurGestionnaire.Text == "" && cbxMois.SelectedIndex != 0)
                {
                    if(cbxMois.Text != mois)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if(cbxMois.SelectedIndex == 0 && cbxEtat.SelectedIndex == 0 && VisiteurGestionnaire.Text == "" && cbxAnnee.SelectedIndex != 0)
                {
                    if(cbxAnnee.Text != annee)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if (cbxMois.SelectedIndex == 0 && cbxEtat.SelectedIndex == 0 && cbxAnnee.SelectedIndex == 0 && VisiteurGestionnaire.Text != "")
                {
                    if (VisiteurGestionnaire.Text != nom)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if (cbxAnnee.SelectedIndex == 0 && cbxMois.SelectedIndex == 0 && VisiteurGestionnaire.Text != "" && cbxEtat.SelectedIndex != 0)
                {
                    if (cbxEtat.Text != etat || VisiteurGestionnaire.Text != nom)
                    {

                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if (cbxAnnee.SelectedIndex == 0 && cbxEtat.SelectedIndex == 0 && cbxMois.SelectedIndex != 0 && VisiteurGestionnaire.Text != "")
                {
                    if (cbxMois.Text != mois || VisiteurGestionnaire.Text != nom)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if (cbxMois.SelectedIndex == 0 && cbxEtat.SelectedIndex == 0 && VisiteurGestionnaire.Text != "" && cbxAnnee.SelectedIndex != 0)
                {
                    if (cbxAnnee.Text != annee || VisiteurGestionnaire.Text != nom)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if (VisiteurGestionnaire.Text != nom && cbxAnnee.SelectedIndex == 0 && cbxMois.SelectedIndex != 0 && cbxEtat.SelectedIndex != 0)
                {
                    if (cbxMois.Text != mois || cbxEtat.Text != etat)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if (cbxMois.SelectedIndex == 0 &&  VisiteurGestionnaire.Text == "" && cbxAnnee.SelectedIndex != 0 && cbxEtat.SelectedIndex != 0)
                {
                    if (cbxAnnee.Text != annee || cbxEtat.Text != etat)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if (VisiteurGestionnaire.Text == "" && cbxEtat.SelectedIndex == 0 && cbxAnnee.SelectedIndex != 0 && cbxMois.SelectedIndex != 0)
                {
                    if (cbxAnnee.Text != annee || cbxMois.Text != mois)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if(cbxAnnee.SelectedIndex == 0 && cbxMois.SelectedIndex != 0 && cbxEtat.SelectedIndex != 0 && VisiteurGestionnaire.Text != "")
                {
                    if(cbxEtat.Text != etat || cbxMois.Text != mois || VisiteurGestionnaire.Text != nom)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if (cbxMois.SelectedIndex == 0 && cbxEtat.SelectedIndex != 0 && VisiteurGestionnaire.Text != "" && cbxAnnee.SelectedIndex != 0)
                {
                    if(cbxAnnee.Text != annee || cbxEtat.Text!= etat || VisiteurGestionnaire.Text != nom)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if(cbxEtat.SelectedIndex == 0 && VisiteurGestionnaire.Text != "" && cbxMois.SelectedIndex != 0 && cbxAnnee.SelectedIndex != 0)
                {
                    if(cbxAnnee.Text != annee || cbxMois.Text != mois || VisiteurGestionnaire.Text != nom)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if (VisiteurGestionnaire.Text == "" && cbxMois.SelectedIndex != 0 && cbxAnnee.SelectedIndex != 0 && cbxEtat.SelectedIndex != 0)
                {
                    if (cbxAnnee.Text != annee || cbxMois.Text != mois || cbxEtat.Text != etat)
                    {
                        dgvFiches.Rows.RemoveAt(ligne);
                        ligne--;
                    }
                }
                else if(cbxAnnee.Text == "Tous" && cbxMois.Text == "Tous" && VisiteurGestionnaire.Text == "" && cbxEtat.Text == "Tous")
                {
                    
                }
                else if(cbxAnnee.Text != annee || cbxMois.Text != mois || cbxEtat.Text != etat || VisiteurGestionnaire.Text != nom)
                {
                    
                    dgvFiches.Rows.RemoveAt(ligne);
                    ligne--;
                }
                
                    ligne++;
            }
        }

        private void cbxEtat_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}

