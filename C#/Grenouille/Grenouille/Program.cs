﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grenouille
{
    class Program
    {
        static void Main(string[] args)
        {
            int profondeur, monte, descend, jours;

            //Saisie de la profondeur du puit
            Console.Write("Quelle est la profondeur du puit ? ");
            profondeur = Convert.ToInt32(Console.ReadLine());

            //Saisie de combien monte la grenouille
            Console.Write("De combien monte la grenouille chaque jour ? ");
            monte = Convert.ToInt32(Console.ReadLine());

            //Saisie de combien elle descend
            Console.Write("De combien elle descend chaque nuit ? ");
            descend = Convert.ToInt32(Console.ReadLine());

            //Calcul
            jours = (int) Math.Ceiling((float)(profondeur - monte) / (monte - descend));
            jours++;
            

            //Affichage du résultat
            Console.Write("La grenouille sort du puit après " + jours + " jours");
            Console.Read();
            
        }
    }
}
