﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classement
{
    class Program
    {
        static void Main(string[] args)
        {
            int nba, nbb, nbc;
            int max=0, inter=0, min=0;

            //Demande des valeurs
            Console.Write("Donnez un nombre : ");
            nba = Convert.ToInt16(Console.ReadLine());
            if (nba > max)
            {
                max = nba;
            }

            Console.Write("Donnez un deuxième nombre : ");
            nbb = Convert.ToInt16(Console.ReadLine());
            if (nbb > max)
            {
                inter = max;
                max = nbb;
            }
            else
            {
                inter = nbb;
            }
            Console.Write("Donnez un troisième nombre : ");
            nbc = Convert.ToInt16(Console.ReadLine());
            if (nbc > max)
            {
                min = inter;
                inter = max;
                max = nbc;

            }
            else if(nbc>inter)
            {
                min = inter;
                inter = nbc;
            }
            else
            {
                min = nbc;
            }
            Console.Write(" " + min + " " + inter + " " + max);
            Console.Read();
        }
    }
}
