﻿namespace motus
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbxNbLettres = new System.Windows.Forms.ComboBox();
            this.txbProposition = new System.Windows.Forms.TextBox();
            this.btnValider = new System.Windows.Forms.Button();
            this.dgvMot = new System.Windows.Forms.DataGridView();
            this.btnPartie = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMot)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre le lettre du mot";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cbxNbLettres
            // 
            this.cbxNbLettres.FormattingEnabled = true;
            this.cbxNbLettres.Location = new System.Drawing.Point(157, 31);
            this.cbxNbLettres.Name = "cbxNbLettres";
            this.cbxNbLettres.Size = new System.Drawing.Size(164, 21);
            this.cbxNbLettres.TabIndex = 1;
            this.cbxNbLettres.SelectedIndexChanged += new System.EventHandler(this.cbxNbLettres_SelectedIndexChanged);
            // 
            // txbProposition
            // 
            this.txbProposition.Location = new System.Drawing.Point(115, 109);
            this.txbProposition.Name = "txbProposition";
            this.txbProposition.Size = new System.Drawing.Size(206, 20);
            this.txbProposition.TabIndex = 2;
            this.txbProposition.TextChanged += new System.EventHandler(this.txbProposition_TextChanged);
            this.txbProposition.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txbProposition_KeyDown_1);
            // 
            // btnValider
            // 
            this.btnValider.Location = new System.Drawing.Point(385, 72);
            this.btnValider.Name = "btnValider";
            this.btnValider.Size = new System.Drawing.Size(114, 57);
            this.btnValider.TabIndex = 3;
            this.btnValider.Text = "Valider";
            this.btnValider.UseVisualStyleBackColor = true;
            this.btnValider.Click += new System.EventHandler(this.btnValider_Click);
            // 
            // dgvMot
            // 
            this.dgvMot.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMot.Location = new System.Drawing.Point(115, 150);
            this.dgvMot.Name = "dgvMot";
            this.dgvMot.Size = new System.Drawing.Size(384, 249);
            this.dgvMot.TabIndex = 4;
            this.dgvMot.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btnPartie
            // 
            this.btnPartie.Location = new System.Drawing.Point(68, 436);
            this.btnPartie.Name = "btnPartie";
            this.btnPartie.Size = new System.Drawing.Size(132, 70);
            this.btnPartie.TabIndex = 5;
            this.btnPartie.Text = "Nouvelle Partie";
            this.btnPartie.UseVisualStyleBackColor = true;
            this.btnPartie.Click += new System.EventHandler(this.btnPartie_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(243, 436);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(132, 70);
            this.btnAnnuler.TabIndex = 6;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.Location = new System.Drawing.Point(416, 436);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(132, 70);
            this.btnQuitter.TabIndex = 7;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 518);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnPartie);
            this.Controls.Add(this.dgvMot);
            this.Controls.Add(this.btnValider);
            this.Controls.Add(this.txbProposition);
            this.Controls.Add(this.cbxNbLettres);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMot)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxNbLettres;
        private System.Windows.Forms.TextBox txbProposition;
        private System.Windows.Forms.Button btnValider;
        private System.Windows.Forms.DataGridView dgvMot;
        private System.Windows.Forms.Button btnPartie;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnQuitter;
    }
}

