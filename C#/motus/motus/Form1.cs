﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace motus
{
    public partial class Form1 : Form
    {
        string motCache;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            for (int i = 5; i < 10; i++)
            {
                cbxNbLettres.Items.Add(i) ;
            }

            cbxNbLettres.SelectedIndex = 0;

            dgvMot.RowCount = 7;
            for(int i =0 ; i < 7; i++)
            {
                dgvMot.Rows[i].Height = 40;
            }


            dgvMot.Height = 7 * 40 + 3;
            dgvMot.Enabled = false;
            dgvMot.ColumnHeadersVisible = false;
            dgvMot.RowHeadersVisible = false;

            btnValider.Enabled = false;
            btnAnnuler.Enabled = false;
            txbProposition.Enabled = false;
            dgvMot.ClearSelection();

        }
        private void txbProposition_KeyDown(object sender, KeyEventArgs e)
        {


            

           

        }

        private void txbProposition_TextChanged(object sender, EventArgs e)
        {
            //forcer la saisie en majuscule
            txbProposition.CharacterCasing = CharacterCasing.Upper;

            //limiter la taille du mot
            txbProposition.MaxLength = Convert.ToInt16(cbxNbLettres.Text);

            //Le bouton se dégrise au nombre de lettre
            if (txbProposition.TextLength == txbProposition.MaxLength)
            {
                btnValider.Enabled = true;
            }
            else
            {
                btnValider.Enabled = false;
            }
        }

        private void cbxNbLettres_SelectedIndexChanged(object sender, EventArgs e)
        {

            txbProposition.Text = null;
            byte nbCol = Convert.ToByte(cbxNbLettres.Text);
            // nb colonnes
            dgvMot.ColumnCount = nbCol;
            dgvMot.Width = 40 * nbCol +3;

            for (int i = 0; i < dgvMot.ColumnCount; i++)
            {
                dgvMot.Columns[i].Width = 40;
                dgvMot.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        int compteur = 0;
        private void btnValider_Click(object sender, EventArgs e)
        {
            string mot = txbProposition.Text;
            Boolean[] dejatraiter = new Boolean[Convert.ToInt32(cbxNbLettres.SelectedItem.ToString())];

            for (int i = 0; i < Convert.ToInt16(cbxNbLettres.Text); i++)
            {
                /************************************************************
                     Recherche des lettres bien placé 
                 ***********************************************************/
                dgvMot[i, compteur].Value = mot[i];
                if (motCache[i] == mot[i])
                {
                    dgvMot.Rows[compteur].Cells[i].Style.BackColor = Color.Green;
                    dejatraiter[i] = true;
                }
            }
            for (int i = 0; i < Convert.ToInt16(cbxNbLettres.Text); i++)
            {
                for (int j = 0; j < Convert.ToInt16(cbxNbLettres.Text); j++)
                {
                    /************************************************************
                     Recherche des lettres mal placé 
                     ***********************************************************/
                    if (motCache[j] == mot[i] && dejatraiter[j] == false)
                    {
                        dgvMot.Rows[compteur].Cells[i].Style.BackColor = Color.Orange;
                        
                    }

                }
            }
            
            compteur++;
            txbProposition.Text = null;

            if (mot == motCache)
            {
                MessageBox.Show("Félicitation ! ");
                btnValider.Enabled = false;
                btnAnnuler.Enabled = false;
                txbProposition.Enabled = false;
                txbProposition.Enabled = false;
                cbxNbLettres.Enabled = true;
                btnPartie.Enabled = true;
                for (int i = 0; i < Convert.ToInt16(cbxNbLettres.Text); i++)
                {
                    for (int j = 0; j < 7; j++)
                    {
                        dgvMot[i, j].Value = null;
                        dgvMot.Rows[j].Cells[i].Style.BackColor = Color.White;
                        compteur = 0;
                    }
                }

            }
            if (compteur == 7)
            {
                MessageBox.Show("Désolé le nombre d'essais est atteint ! Le mot était : " + motCache );
                btnValider.Enabled = false;
                btnAnnuler.Enabled = false;
                txbProposition.Enabled = false;
                txbProposition.Enabled = false;
                cbxNbLettres.Enabled = true;
                btnPartie.Enabled = true;
                for (int i = 0; i < Convert.ToInt16(cbxNbLettres.Text); i++)
                {
                    for (int j = 0; j < 7; j++)
                    {
                        dgvMot[i, j].Value = null;
                        dgvMot.Rows[j].Cells[i].Style.BackColor = Color.White;
                        compteur = 0;
                    }
                }
            }
           
            
        }

        private void btnPartie_Click(object sender, EventArgs e)
        {
            btnValider.Enabled = false;
            btnAnnuler.Enabled = true;
            btnPartie.Enabled = false;
            txbProposition.Enabled = true;
            cbxNbLettres.Enabled = false;
            Random tirage = new Random();

            string path = "..\\..\\..\\";
            string nomFichier = "mots" + cbxNbLettres.SelectedItem + ".txt";
            path = path + nomFichier;

            int nbMots = File.ReadAllLines(path).Count();

            string[] lesLignes = File.ReadAllLines(path);

            int positionMot = tirage.Next(0, nbMots);

            motCache = lesLignes[positionMot];

            MessageBox.Show(motCache);

            //Première lettre
            dgvMot[0, 0].Value = motCache[0];
            dgvMot.Rows[0].Cells[0].Style.BackColor = Color.Green;

            

        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            btnValider.Enabled = false;
            btnAnnuler.Enabled = false;
            txbProposition.Enabled = false;
            cbxNbLettres.Enabled = true;
            btnPartie.Enabled = true;


            for (int i = 0; i < Convert.ToInt16(cbxNbLettres.Text); i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    dgvMot[i,j].Value = null;
                    dgvMot.Rows[j].Cells[i].Style.BackColor = Color.White;
                    compteur = 0;
                }
            }
                
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txbProposition_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (((int)e.KeyCode >= 1 && (int)e.KeyCode <= 7)
            || ((int)e.KeyCode >= 9 && (int)e.KeyCode <= 36)
            || ((int)e.KeyCode >= 40 && (int)e.KeyCode <= 45)
            || ((int)e.KeyCode >= 47 && (int)e.KeyCode <= 64)
            || ((int)e.KeyCode == 38)
            || ((int)e.KeyCode > 90)
            )
            {
                e.SuppressKeyPress = true;
            }
        }
    }
}
