﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NombreCaché
{
    class Program
    {
        static void Main(string[] args)
        {
            double res = 0;
            int i = 0;
            Random rnd = new Random();
            int mystere = rnd.Next(0, 101);
            int nb;

            Console.WriteLine("Devinez à quel chiffre je pense, entre 0 et 100 ");
            Console.WriteLine("Vous avez 7 essais, bonne chance !");
            try
            {
                nb = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Veuillez rentrez un nombre");
                nb = Convert.ToInt32(Console.ReadLine());
            }
            while (res != 1 && i < 6)
            {
                if (mystere == nb)
                {
                    res = 1;
                    Console.WriteLine("Félicitations ! Mon nombre était bien " + mystere);
                    Console.Read();
                }
                else if (nb > mystere)
                {
                    Console.WriteLine("Plus petit ");
                    nb = Convert.ToInt32(Console.ReadLine());
                }
                else if (nb < mystere)
                {
                    Console.WriteLine("Plus grand ");
                    nb = Convert.ToInt32(Console.ReadLine());
                }
                i = i + 1;
                if (i == 6)
                {
                    res = 1;
                    Console.WriteLine("Perdu ! Mon nombre était " + mystere);
                    Console.ReadLine();
                }
            }
        }
    }
}
