﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Emprunt
{
    public partial class frmEmprunt : Form
    {
        public frmEmprunt()
        {
            InitializeComponent();
        }
        float[] tablTaux = new float[20];
        int[] tablDuree = new int[80];
        private void Form1_Load(object sender, EventArgs e)
        {
            const byte NB_COL = 6;
            float taux;
            int nbItemsDuree = 40, nbItemsTaux = 20, duree;



            for (int i = 0; i < nbItemsTaux; i++)
            {
                taux = (float)((1 + i) * 0.5);
                tablTaux[i] = taux;

                cbxTaux.Items.Add(taux + " %");
            }
            cbxTaux.SelectedIndex = 0;



            for (int i = 0; i < nbItemsDuree; i++)
            {
                duree = 3 + i * 3;
                tablDuree[i] = duree;

                cbxDuree.Items.Add(duree + " mois");
            }
            cbxDuree.SelectedIndex = 0;



            dgvTabAmort.ColumnCount = NB_COL;
            dgvTabAmort.Width = 140 * NB_COL + 6;




            dgvTabAmort.ColumnHeadersVisible = true;
            dgvTabAmort.Columns[0].HeaderText = "Date";
            dgvTabAmort.Columns[1].HeaderText = "Echéance";
            dgvTabAmort.Columns[2].HeaderText = "Amortissement";
            dgvTabAmort.Columns[3].HeaderText = "Intérêts";
            dgvTabAmort.Columns[4].HeaderText = "Assurance";
            dgvTabAmort.Columns[5].HeaderText = "Capital restant dû";



            dgvTabAmort.ReadOnly = true;
            dgvTabAmort.RowHeadersVisible = false;




            for (int i = 0; i < dgvTabAmort.ColumnCount; i++)
            {
                dgvTabAmort.ColumnHeadersDefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleCenter;
                dgvTabAmort.Columns[i].SortMode =
                DataGridViewColumnSortMode.NotSortable;
                dgvTabAmort.Columns[i].Width = 140;
                dgvTabAmort.Columns[i].DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.BottomRight;
            }

        }

        private void btnValider_Click(object sender, EventArgs e)
        {
            int duree;
            double mensualite, montant, taux, interet, amortissement;
            DateTime dateEcheance = dtpDate.Value;


            taux = tablTaux[cbxTaux.SelectedIndex] / 100;
            duree = tablDuree[Convert.ToInt32(cbxDuree.SelectedIndex)];
            dgvTabAmort.RowCount = 0;
            //dtpDate.Format = DateTimePickerFormat.Custom;
            //dtpDate.CustomFormat = "dd-MM-yyyy";




            try
            {
                montant = Convert.ToSingle(tbxMontant.Text);
                mensualite = (montant * (taux / 12)) / (1 - Math.Pow((1 + taux / 12), -duree));
                

                lblMontantMensualite.Text = mensualite.ToString("N2") + " €";
                
                while(montant >= 0) {

                    for (int j = 0; j < duree; j++)
                    {
                        dgvTabAmort.Rows.Add();
                        //Assurance
                        dgvTabAmort[4, j].Value = txtAssurance.Text + " €";
                        //Echéance
                        dgvTabAmort[1, j].Value = (Convert.ToInt16(txtAssurance.Text) + mensualite).ToString("N2") + " €";
                        //Date
                        dgvTabAmort[0, j].Value = dateEcheance.ToString("d");
                        dateEcheance = dtpDate.Value.AddMonths(j + 1);
                        //Interêt
                        interet = montant * taux / 12;
                        dgvTabAmort[3, j].Value = interet.ToString("N2") +" €";
                        //Capital
                        montant = montant - mensualite + interet;
                        dgvTabAmort[5, j].Value = montant.ToString("N2") + " €";
                        //Amortissement
                        amortissement = mensualite - interet;
                        dgvTabAmort[2, j].Value = amortissement.ToString("N2") + " €";


                    }
                }
            }
	catch{
		MessageBox.Show("Le montant et l'assurance doivent être un nombre", "Erreur");
		tbxMontant.Text = null;
        txtAssurance.Text = null;
		tbxMontant.Focus();
	}

        }

        private void tbxMontant_TextChanged(object sender, EventArgs e)
        {
            lblMontantMensualite.Text = "";
            dgvTabAmort.RowCount = 0;
            ;
        }

        private void cbxTaux_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMontantMensualite.Text = null;
        }

        private void CbxDuree_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMontantMensualite.Text = null;
        }

        private void BtnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void BtnEffacer_Click(object sender, EventArgs e)
        {
            tbxMontant.Text = null;
            lblMontantMensualite.Text = null;
            txtAssurance.Text = null;
            dtpDate.Text = null;
            cbxDuree.SelectedIndex = 0;
            cbxTaux.SelectedIndex = 0;
            
        }

        private void DtpDate_ValueChanged(object sender, EventArgs e)
        {
            lblMontantMensualite.Text = null;
        }

        private void lblDuree_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void lblMontantMensualite_Click(object sender, EventArgs e)
        {

        }

        private void dgvTabAmort_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtAssurance_TextChanged(object sender, EventArgs e)
        {
            lblMontantMensualite.Text = null;
        }
    }
}
