﻿namespace Emprunt
{
    partial class frmEmprunt
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalculer = new System.Windows.Forms.Button();
            this.lblMontant = new System.Windows.Forms.Label();
            this.tbxMontant = new System.Windows.Forms.TextBox();
            this.lblDuree = new System.Windows.Forms.Label();
            this.lblTaux = new System.Windows.Forms.Label();
            this.cbxTaux = new System.Windows.Forms.ComboBox();
            this.cbxDuree = new System.Windows.Forms.ComboBox();
            this.lblTitre = new System.Windows.Forms.Label();
            this.lblMensualite = new System.Windows.Forms.Label();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.lblMontantMensualite = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnEffacer = new System.Windows.Forms.Button();
            this.dgvTabAmort = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAssurance = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabAmort)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCalculer
            // 
            this.btnCalculer.Location = new System.Drawing.Point(153, 327);
            this.btnCalculer.Name = "btnCalculer";
            this.btnCalculer.Size = new System.Drawing.Size(119, 34);
            this.btnCalculer.TabIndex = 0;
            this.btnCalculer.Text = "Calculer";
            this.btnCalculer.UseVisualStyleBackColor = true;
            this.btnCalculer.Click += new System.EventHandler(this.btnValider_Click);
            // 
            // lblMontant
            // 
            this.lblMontant.AutoSize = true;
            this.lblMontant.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMontant.Location = new System.Drawing.Point(256, 53);
            this.lblMontant.Name = "lblMontant";
            this.lblMontant.Size = new System.Drawing.Size(69, 18);
            this.lblMontant.TabIndex = 1;
            this.lblMontant.Text = "Montant";
            // 
            // tbxMontant
            // 
            this.tbxMontant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxMontant.Location = new System.Drawing.Point(389, 53);
            this.tbxMontant.Name = "tbxMontant";
            this.tbxMontant.Size = new System.Drawing.Size(178, 26);
            this.tbxMontant.TabIndex = 2;
            this.tbxMontant.TextChanged += new System.EventHandler(this.tbxMontant_TextChanged);
            // 
            // lblDuree
            // 
            this.lblDuree.AutoSize = true;
            this.lblDuree.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuree.Location = new System.Drawing.Point(256, 150);
            this.lblDuree.Name = "lblDuree";
            this.lblDuree.Size = new System.Drawing.Size(51, 18);
            this.lblDuree.TabIndex = 3;
            this.lblDuree.Text = "Durée";
            this.lblDuree.Click += new System.EventHandler(this.lblDuree_Click);
            // 
            // lblTaux
            // 
            this.lblTaux.AutoSize = true;
            this.lblTaux.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaux.Location = new System.Drawing.Point(256, 101);
            this.lblTaux.Name = "lblTaux";
            this.lblTaux.Size = new System.Drawing.Size(45, 18);
            this.lblTaux.TabIndex = 4;
            this.lblTaux.Text = "Taux";
            // 
            // cbxTaux
            // 
            this.cbxTaux.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxTaux.FormattingEnabled = true;
            this.cbxTaux.Location = new System.Drawing.Point(389, 101);
            this.cbxTaux.Name = "cbxTaux";
            this.cbxTaux.Size = new System.Drawing.Size(178, 28);
            this.cbxTaux.TabIndex = 5;
            this.cbxTaux.SelectedIndexChanged += new System.EventHandler(this.cbxTaux_SelectedIndexChanged);
            // 
            // cbxDuree
            // 
            this.cbxDuree.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxDuree.FormattingEnabled = true;
            this.cbxDuree.Location = new System.Drawing.Point(389, 150);
            this.cbxDuree.Name = "cbxDuree";
            this.cbxDuree.Size = new System.Drawing.Size(178, 28);
            this.cbxDuree.TabIndex = 6;
            this.cbxDuree.SelectedIndexChanged += new System.EventHandler(this.CbxDuree_SelectedIndexChanged);
            // 
            // lblTitre
            // 
            this.lblTitre.AutoSize = true;
            this.lblTitre.Font = new System.Drawing.Font("Georgia", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitre.Location = new System.Drawing.Point(225, 9);
            this.lblTitre.Name = "lblTitre";
            this.lblTitre.Size = new System.Drawing.Size(342, 29);
            this.lblTitre.TabIndex = 8;
            this.lblTitre.Text = "Remboursement d\'un emprunt";
            // 
            // lblMensualite
            // 
            this.lblMensualite.AutoSize = true;
            this.lblMensualite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensualite.Location = new System.Drawing.Point(256, 274);
            this.lblMensualite.Name = "lblMensualite";
            this.lblMensualite.Size = new System.Drawing.Size(86, 18);
            this.lblMensualite.TabIndex = 9;
            this.lblMensualite.Text = "Mensualité";
            // 
            // btnQuitter
            // 
            this.btnQuitter.Location = new System.Drawing.Point(515, 327);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(116, 34);
            this.btnQuitter.TabIndex = 10;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.BtnQuitter_Click);
            // 
            // lblMontantMensualite
            // 
            this.lblMontantMensualite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMontantMensualite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMontantMensualite.Location = new System.Drawing.Point(389, 264);
            this.lblMontantMensualite.Name = "lblMontantMensualite";
            this.lblMontantMensualite.Size = new System.Drawing.Size(178, 28);
            this.lblMontantMensualite.TabIndex = 11;
            this.lblMontantMensualite.Click += new System.EventHandler(this.lblMontantMensualite_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(256, 194);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(81, 18);
            this.label.TabIndex = 12;
            this.label.Text = "Assurance";
            this.label.Click += new System.EventHandler(this.label1_Click);
            // 
            // dtpDate
            // 
            this.dtpDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Location = new System.Drawing.Point(389, 232);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(178, 20);
            this.dtpDate.TabIndex = 13;
            this.dtpDate.ValueChanged += new System.EventHandler(this.DtpDate_ValueChanged);
            // 
            // btnEffacer
            // 
            this.btnEffacer.Location = new System.Drawing.Point(344, 327);
            this.btnEffacer.Name = "btnEffacer";
            this.btnEffacer.Size = new System.Drawing.Size(119, 34);
            this.btnEffacer.TabIndex = 14;
            this.btnEffacer.Text = "Effacer";
            this.btnEffacer.UseVisualStyleBackColor = true;
            this.btnEffacer.Click += new System.EventHandler(this.BtnEffacer_Click);
            // 
            // dgvTabAmort
            // 
            this.dgvTabAmort.AllowUserToAddRows = false;
            this.dgvTabAmort.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTabAmort.Location = new System.Drawing.Point(83, 394);
            this.dgvTabAmort.Name = "dgvTabAmort";
            this.dgvTabAmort.ReadOnly = true;
            this.dgvTabAmort.RowHeadersVisible = false;
            this.dgvTabAmort.Size = new System.Drawing.Size(641, 157);
            this.dgvTabAmort.TabIndex = 15;
            this.dgvTabAmort.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTabAmort_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(256, 232);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 18);
            this.label1.TabIndex = 16;
            this.label1.Text = "Date de début";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // txtAssurance
            // 
            this.txtAssurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssurance.Location = new System.Drawing.Point(389, 194);
            this.txtAssurance.Name = "txtAssurance";
            this.txtAssurance.Size = new System.Drawing.Size(178, 26);
            this.txtAssurance.TabIndex = 17;
            this.txtAssurance.TextChanged += new System.EventHandler(this.txtAssurance_TextChanged);
            // 
            // frmEmprunt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 582);
            this.Controls.Add(this.txtAssurance);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvTabAmort);
            this.Controls.Add(this.btnEffacer);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.label);
            this.Controls.Add(this.lblMontantMensualite);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.lblMensualite);
            this.Controls.Add(this.lblTitre);
            this.Controls.Add(this.cbxDuree);
            this.Controls.Add(this.cbxTaux);
            this.Controls.Add(this.lblTaux);
            this.Controls.Add(this.lblDuree);
            this.Controls.Add(this.tbxMontant);
            this.Controls.Add(this.lblMontant);
            this.Controls.Add(this.btnCalculer);
            this.Name = "frmEmprunt";
            this.Text = "Emprunt";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabAmort)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalculer;
        private System.Windows.Forms.Label lblMontant;
        private System.Windows.Forms.TextBox tbxMontant;
        private System.Windows.Forms.Label lblDuree;
        private System.Windows.Forms.Label lblTaux;
        private System.Windows.Forms.ComboBox cbxTaux;
        private System.Windows.Forms.ComboBox cbxDuree;
        private System.Windows.Forms.Label lblTitre;
        private System.Windows.Forms.Label lblMensualite;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Label lblMontantMensualite;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Button btnEffacer;
        private System.Windows.Forms.DataGridView dgvTabAmort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAssurance;
    }
}

