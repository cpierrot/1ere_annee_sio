﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Mail
{
    public partial class AjouterContact : Form
    {
        public AjouterContact()

        {
            InitializeComponent();
            
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            string fichierContacts = @".../.../.../contact.txt";
            StreamWriter contactSW;
            //(this.Owner as Form1).Controls["tbxDestinataire"].Text = tbxMail.Text;
            

            string ligne;
            if (!File.Exists(fichierContacts))
            {
                contactSW = new StreamWriter(fichierContacts);
            }
            else
            {
                contactSW = File.AppendText(fichierContacts);
            }
            ligne = (tbxNom.Text + ";" + tbxMail.Text + ";" + tbxTel.Text + ";");
            contactSW.WriteLine(ligne);

            contactSW.Close();
            contactSW.Dispose();

            (this.Owner as Contact).ajoutLigne();

            this.Close();
        }

        private void AjouterContact_Load(object sender, EventArgs e)
        {

        }
    }
}
