﻿namespace Mail
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbxServeur = new System.Windows.Forms.ComboBox();
            this.tbxDestinataire = new System.Windows.Forms.TextBox();
            this.tbxCopie = new System.Windows.Forms.TextBox();
            this.tbxExpediteur = new System.Windows.Forms.TextBox();
            this.tbxMessage = new System.Windows.Forms.TextBox();
            this.tbxPiecesJointes = new System.Windows.Forms.TextBox();
            this.tbxObjet = new System.Windows.Forms.TextBox();
            this.btnServeur = new System.Windows.Forms.Button();
            this.btnAjouterD = new System.Windows.Forms.Button();
            this.btnAjouterC = new System.Windows.Forms.Button();
            this.btnAjouterPJ = new System.Windows.Forms.Button();
            this.btnEnvoyer = new System.Windows.Forms.Button();
            this.btnContact = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.BtnQuitter = new System.Windows.Forms.Button();
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxCopieC = new System.Windows.Forms.TextBox();
            this.btnAjouterCC = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(75, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(252, 45);
            this.label1.TabIndex = 0;
            this.label1.Text = "Paloma Express";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Serveur SMTP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 277);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Objet";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 313);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Pièces jointes";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 237);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Expéditeur";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 351);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Message";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(38, 164);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Cc";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Destinataire";
            // 
            // cbxServeur
            // 
            this.cbxServeur.FormattingEnabled = true;
            this.cbxServeur.Location = new System.Drawing.Point(150, 73);
            this.cbxServeur.Name = "cbxServeur";
            this.cbxServeur.Size = new System.Drawing.Size(177, 21);
            this.cbxServeur.TabIndex = 9;
            this.cbxServeur.SelectedIndexChanged += new System.EventHandler(this.cbxServeur_SelectedIndexChanged);
            // 
            // tbxDestinataire
            // 
            this.tbxDestinataire.Location = new System.Drawing.Point(150, 114);
            this.tbxDestinataire.Name = "tbxDestinataire";
            this.tbxDestinataire.Size = new System.Drawing.Size(177, 20);
            this.tbxDestinataire.TabIndex = 10;
            // 
            // tbxCopie
            // 
            this.tbxCopie.Location = new System.Drawing.Point(150, 157);
            this.tbxCopie.Name = "tbxCopie";
            this.tbxCopie.Size = new System.Drawing.Size(177, 20);
            this.tbxCopie.TabIndex = 11;
            // 
            // tbxExpediteur
            // 
            this.tbxExpediteur.Location = new System.Drawing.Point(150, 237);
            this.tbxExpediteur.Name = "tbxExpediteur";
            this.tbxExpediteur.Size = new System.Drawing.Size(177, 20);
            this.tbxExpediteur.TabIndex = 13;
            // 
            // tbxMessage
            // 
            this.tbxMessage.Location = new System.Drawing.Point(150, 351);
            this.tbxMessage.Multiline = true;
            this.tbxMessage.Name = "tbxMessage";
            this.tbxMessage.Size = new System.Drawing.Size(268, 98);
            this.tbxMessage.TabIndex = 14;
            // 
            // tbxPiecesJointes
            // 
            this.tbxPiecesJointes.Location = new System.Drawing.Point(150, 306);
            this.tbxPiecesJointes.Name = "tbxPiecesJointes";
            this.tbxPiecesJointes.Size = new System.Drawing.Size(177, 20);
            this.tbxPiecesJointes.TabIndex = 15;
            // 
            // tbxObjet
            // 
            this.tbxObjet.Location = new System.Drawing.Point(150, 270);
            this.tbxObjet.Name = "tbxObjet";
            this.tbxObjet.Size = new System.Drawing.Size(177, 20);
            this.tbxObjet.TabIndex = 16;
            // 
            // btnServeur
            // 
            this.btnServeur.Location = new System.Drawing.Point(366, 73);
            this.btnServeur.Name = "btnServeur";
            this.btnServeur.Size = new System.Drawing.Size(75, 23);
            this.btnServeur.TabIndex = 17;
            this.btnServeur.Text = "Serveur";
            this.btnServeur.UseVisualStyleBackColor = true;
            this.btnServeur.Click += new System.EventHandler(this.btnServeur_Click);
            // 
            // btnAjouterD
            // 
            this.btnAjouterD.Location = new System.Drawing.Point(366, 111);
            this.btnAjouterD.Name = "btnAjouterD";
            this.btnAjouterD.Size = new System.Drawing.Size(75, 23);
            this.btnAjouterD.TabIndex = 18;
            this.btnAjouterD.Text = "Ajouter";
            this.btnAjouterD.UseVisualStyleBackColor = true;
            this.btnAjouterD.Click += new System.EventHandler(this.btnAjouterD_Click);
            // 
            // btnAjouterC
            // 
            this.btnAjouterC.Location = new System.Drawing.Point(366, 154);
            this.btnAjouterC.Name = "btnAjouterC";
            this.btnAjouterC.Size = new System.Drawing.Size(75, 23);
            this.btnAjouterC.TabIndex = 19;
            this.btnAjouterC.Text = "Ajouter";
            this.btnAjouterC.UseVisualStyleBackColor = true;
            this.btnAjouterC.Click += new System.EventHandler(this.btnAjouterC_Click);
            // 
            // btnAjouterPJ
            // 
            this.btnAjouterPJ.Location = new System.Drawing.Point(366, 303);
            this.btnAjouterPJ.Name = "btnAjouterPJ";
            this.btnAjouterPJ.Size = new System.Drawing.Size(75, 23);
            this.btnAjouterPJ.TabIndex = 21;
            this.btnAjouterPJ.Text = "Ajouter";
            this.btnAjouterPJ.UseVisualStyleBackColor = true;
            this.btnAjouterPJ.Click += new System.EventHandler(this.btnAjouterPJ_Click);
            // 
            // btnEnvoyer
            // 
            this.btnEnvoyer.Location = new System.Drawing.Point(35, 480);
            this.btnEnvoyer.Name = "btnEnvoyer";
            this.btnEnvoyer.Size = new System.Drawing.Size(75, 23);
            this.btnEnvoyer.TabIndex = 22;
            this.btnEnvoyer.Text = "Envoyer";
            this.btnEnvoyer.UseVisualStyleBackColor = true;
            this.btnEnvoyer.Click += new System.EventHandler(this.btnEnvoyer_Click);
            // 
            // btnContact
            // 
            this.btnContact.Location = new System.Drawing.Point(150, 480);
            this.btnContact.Name = "btnContact";
            this.btnContact.Size = new System.Drawing.Size(75, 23);
            this.btnContact.TabIndex = 23;
            this.btnContact.Text = "Contact";
            this.btnContact.UseVisualStyleBackColor = true;
            this.btnContact.Click += new System.EventHandler(this.btnContact_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(261, 480);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 23);
            this.btnAnnuler.TabIndex = 24;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // BtnQuitter
            // 
            this.BtnQuitter.Location = new System.Drawing.Point(366, 480);
            this.BtnQuitter.Name = "BtnQuitter";
            this.BtnQuitter.Size = new System.Drawing.Size(75, 23);
            this.BtnQuitter.TabIndex = 25;
            this.BtnQuitter.Text = "Quitter";
            this.BtnQuitter.UseVisualStyleBackColor = true;
            this.BtnQuitter.Click += new System.EventHandler(this.BtnQuitter_Click);
            // 
            // OFD
            // 
            this.OFD.FileName = "openFileDialog1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 199);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Cci";
            // 
            // tbxCopieC
            // 
            this.tbxCopieC.Location = new System.Drawing.Point(150, 199);
            this.tbxCopieC.Name = "tbxCopieC";
            this.tbxCopieC.Size = new System.Drawing.Size(177, 20);
            this.tbxCopieC.TabIndex = 12;
            // 
            // btnAjouterCC
            // 
            this.btnAjouterCC.Location = new System.Drawing.Point(366, 196);
            this.btnAjouterCC.Name = "btnAjouterCC";
            this.btnAjouterCC.Size = new System.Drawing.Size(75, 23);
            this.btnAjouterCC.TabIndex = 20;
            this.btnAjouterCC.Text = "Ajouter";
            this.btnAjouterCC.UseVisualStyleBackColor = true;
            this.btnAjouterCC.Click += new System.EventHandler(this.btnAjouterCC_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 525);
            this.Controls.Add(this.BtnQuitter);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnContact);
            this.Controls.Add(this.btnEnvoyer);
            this.Controls.Add(this.btnAjouterPJ);
            this.Controls.Add(this.btnAjouterCC);
            this.Controls.Add(this.btnAjouterC);
            this.Controls.Add(this.btnAjouterD);
            this.Controls.Add(this.btnServeur);
            this.Controls.Add(this.tbxObjet);
            this.Controls.Add(this.tbxPiecesJointes);
            this.Controls.Add(this.tbxMessage);
            this.Controls.Add(this.tbxExpediteur);
            this.Controls.Add(this.tbxCopieC);
            this.Controls.Add(this.tbxCopie);
            this.Controls.Add(this.tbxDestinataire);
            this.Controls.Add(this.cbxServeur);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbxServeur;
        private System.Windows.Forms.TextBox tbxDestinataire;
        private System.Windows.Forms.TextBox tbxCopie;
        private System.Windows.Forms.TextBox tbxExpediteur;
        private System.Windows.Forms.TextBox tbxMessage;
        private System.Windows.Forms.TextBox tbxPiecesJointes;
        private System.Windows.Forms.TextBox tbxObjet;
        private System.Windows.Forms.Button btnServeur;
        private System.Windows.Forms.Button btnAjouterD;
        private System.Windows.Forms.Button btnAjouterC;
        private System.Windows.Forms.Button btnAjouterPJ;
        private System.Windows.Forms.Button btnEnvoyer;
        private System.Windows.Forms.Button btnContact;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button BtnQuitter;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxCopieC;
        private System.Windows.Forms.Button btnAjouterCC;
    }
}

