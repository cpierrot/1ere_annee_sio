﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Mail
{
    public partial class Serveur : Form
    {
        public Serveur()
        {
            InitializeComponent();
        }

        private void Serveur_Load(object sender, EventArgs e)
        {
            string fichierServeur = @".../.../.../serveur.txt";
            StreamReader serveurSR;
            string liste;
            int i = 0;

            ////  Remplir la liste des serveurs ////
            serveurSR = new StreamReader(fichierServeur);
            while ((liste = serveurSR.ReadLine()) != null)             
            {
                string[] tabLigne = liste.Split(';');
                while(i < tabLigne.Length - 1)
                {
                    lbxSMTP.Items.Add(tabLigne[i]);
                    i++;
                }
            }
            serveurSR.Close();
            serveurSR.Dispose();

        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            string fichierServeur = @".../.../.../serveur.txt";
            StreamWriter serveurSW;

            string ligne;
            if (!File.Exists(fichierServeur))
            {
                serveurSW = new StreamWriter(fichierServeur);
                
            }
            else
            {
                serveurSW = File.AppendText(fichierServeur);
            }
            ligne = (";" + tbxSMTP.Text);
            serveurSW.Write(ligne);

            serveurSW.Close();
            serveurSW.Dispose();

            
            (this.Owner as Form1).Controls["cbxServeur"].Text = tbxSMTP.Text;
            this.Close();
        }

        private void tbxSMTP_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            string ligne;
            string fichier = @".../.../.../serveur.txt";
            string fichier2 = @".../.../.../serveur2.txt";

            StreamReader SR = new StreamReader(fichier);
            StreamWriter SW = new StreamWriter(fichier2);
            while ((ligne = SR.ReadLine()) != null)
            {
                string[] tabLigne = ligne.Split(';');
                if (Convert.ToString(tabLigne[0]) != this.lbxSMTP.SelectedItem.ToString())
                {
                    SW.WriteLine(ligne);
                }
            }
            SR.Close();
            SR.Dispose();
            SW.Close();
            SW.Dispose();

            File.Replace(fichier2, fichier, @".../.../.../serveurSave.txt");
            lbxSMTP.Items.Remove(lbxSMTP.SelectedItem);
            MessageBox.Show("Serveur supprimé");
            try
            {
                

            }
            catch (Exception)
            {
                MessageBox.Show("Impossible de supprimer ce serveur");
            }
        }
    }
}
