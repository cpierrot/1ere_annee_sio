﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.IO;

namespace Mail
{
    public partial class Form1 : Form
    {
        DialogResult result;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void BtnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEnvoyer_Click(object sender, EventArgs e)
        {
            
            SmtpClient clientSmtp = new SmtpClient(Convert.ToString(cbxServeur.SelectedItem));
            MailMessage monMessage = new MailMessage();


            monMessage.Body = tbxMessage.Text;
            monMessage.Subject = tbxObjet.Text;

            MailAddress destinataire = new MailAddress(tbxDestinataire.Text);
            monMessage.To.Add(destinataire);

            MailAddress copie = new MailAddress(tbxCopie.Text);
            monMessage.To.Add(copie);

            MailAddress expediteur = new MailAddress(tbxExpediteur.Text);
            monMessage.From = expediteur;

            if(tbxPiecesJointes.Enabled == false)
            {
                monMessage.Attachments.Add(new Attachment(OFD.FileName));
            }

            MailAddress cci = new MailAddress(tbxCopieC.Text);
            monMessage.To.Add(cci); 


            try
            {
                clientSmtp.Send(monMessage);
                MessageBox.Show("Le message a été envoyé");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Echec : " + ex.Message);
            }

        }

        private void btnAjouterD_Click(object sender, EventArgs e)
        {
            Contact contact = new Contact();
            contact.Owner = this;
            contact.ShowDialog();

        }

        private void btnContact_Click(object sender, EventArgs e)
        {
            Contact contact = new Contact();
            contact.Owner = this;
            contact.ShowDialog();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            tbxDestinataire.Text = null;
            tbxCopie.Text = null;
            tbxCopieC.Text = null;
            tbxExpediteur.Text = null;
            tbxMessage.Text = null;
            tbxObjet.Text = null;
            tbxPiecesJointes.Text = null;
        }

        private void btnAjouterC_Click(object sender, EventArgs e)
        {
            Contact contact = new Contact();
            contact.Owner = this;
            contact.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            string fichierServeur = @".../.../.../serveur.txt";
            StreamReader serveurSR = new StreamReader(@fichierServeur);
            string ligne;
            int i = 0;

            while ((ligne = serveurSR.ReadLine()) != null)
            {
                
                string[] tabLigne = ligne.Split(';');
                
                for(i = 0; i < tabLigne.Length; i++){
                    cbxServeur.Items.Add(tabLigne[i]);
                }
                

            }
            serveurSR.Close();
            serveurSR.Dispose();
        }

        private void btnAjouterCC_Click(object sender, EventArgs e)
        {
            Contact contact = new Contact();
            contact.Owner = this;
            contact.ShowDialog();
        }

        private void btnServeur_Click(object sender, EventArgs e)
        {
            Serveur serveur = new Serveur();
            serveur.Owner = this;
            serveur.ShowDialog();
        }

        private void cbxServeur_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void btnAjouterPJ_Click(object sender, EventArgs e)
        {
            String ligne;
            OFD.Title = "Pièce jointe"; //titre de la boite de dialogue
            OFD.FileName = String.Empty;

            if (OFD.ShowDialog() == (result = System.Windows.Forms.DialogResult.OK))
            {


                try
                {
                    
                    StreamReader SR = new StreamReader(OFD.OpenFile()); // Ouvrir le flux en lecture
                    while ((ligne = SR.ReadLine()) != null) //lire le fichier ligne par ligne 
                    {
                        tbxPiecesJointes.Text = OFD.FileName; // copier les lignes du fichier dans la listBox

                    }
                    SR.Close(); // fermer le flux
                    tbxPiecesJointes.Enabled = false;
                }
                catch
                {
                    MessageBox.Show("Erreur: Lecture du fichier impossible");
                }
            }
        }
    }
}

