﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Mail
{
    public partial class Contact : Form
    {
        int compteur = 0;
        public Contact()
        {
            InitializeComponent();
            dgvContact.ColumnCount = 3;
            this.dgvContact.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvContact.MultiSelect = true;

        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            AjouterContact ajouterContact = new AjouterContact();
            ajouterContact.Owner = this;
            ajouterContact.ShowDialog();
        }

        private void Contact_Load(object sender, EventArgs e)
        {
            dgvContact.ColumnHeadersVisible = true;
            dgvContact.Columns[0].HeaderText = "Nom";
            dgvContact.Columns[1].HeaderText = "Adresse mail";
            dgvContact.Columns[2].HeaderText = "Téléphone";

            string fichierContacts = @".../.../.../contact.txt";
            StreamReader contactSR = new StreamReader(@fichierContacts);
            string ligne;
            int i = 0;

            while ((ligne=contactSR.ReadLine()) != null)
            {
                dgvContact.Rows.Add();
                string [] tabLigne = ligne.Split(';');
                dgvContact[0, compteur].Value = tabLigne[0];
                dgvContact[1, compteur].Value = tabLigne[1];
                dgvContact[2, compteur].Value = tabLigne[2];
                i++;
                compteur++;
                
            }

            
            contactSR.Close();
            contactSR.Dispose();
        }

        public void ajoutLigne()
        {
            
            string fichierContacts = @".../.../.../contact.txt";
            StreamReader contactSR = new StreamReader(@fichierContacts);
            string ligne;
            int i = 0;

            while ((ligne = contactSR.ReadLine()) != null)
            {
                dgvContact.Rows.Add();
                string[] tabLigne = ligne.Split(';');
                dgvContact[0, compteur].Value = tabLigne[0];
                dgvContact[1, compteur].Value = tabLigne[1];
                dgvContact[2, compteur].Value = tabLigne[2];
                i++;

            }
            contactSR.Close();
            contactSR.Dispose();
        }

        private void dgvContact_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void btnSelection_Click(object sender, EventArgs e)
        {
            
            String valeurCellule = Convert.ToString(dgvContact.CurrentRow.Cells[1].Value);
            (this.Owner as Form1).Controls["tbxDestinataire"].Text = valeurCellule;
            this.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String valeurCellule = Convert.ToString(dgvContact.CurrentRow.Cells[1].Value);
            (this.Owner as Form1).Controls["tbxCopie"].Text = valeurCellule;
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string ligne;
            string fichier = @".../.../.../contact.txt";
            string fichier2 = @".../.../.../contact2.txt";
            
            
            try
            {
                StreamReader SR = new StreamReader(fichier);
                StreamWriter SW = new StreamWriter(fichier2);
                while ((ligne = SR.ReadLine()) != null)
                {
                    string[] tabLigne = ligne.Split(';');
                    if (Convert.ToString(tabLigne[0]) != this.dgvContact.SelectedRows[0].Cells[0].Value.ToString())
                    {
                        SW.WriteLine(ligne);
                    }
                }
                SR.Close();
                SR.Dispose();
                SW.Close();
                SW.Dispose();

                File.Replace(fichier2, fichier, @".../.../.../contactSave.txt");
                dgvContact.Rows.RemoveAt(dgvContact.SelectedRows[0].Index);
                MessageBox.Show("Contact supprimé");

            }
            catch (Exception)
            {
                MessageBox.Show("Impossible de supprimer ce contact");
            }
            
            
        }

        private void btncci_Click(object sender, EventArgs e)
        {
            String valeurCellule = Convert.ToString(dgvContact.CurrentRow.Cells[1].Value);
            (this.Owner as Form1).Controls["tbxCopieC"].Text = valeurCellule;
            this.Close();
        }
    }
}
