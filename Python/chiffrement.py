# ord("A") renvoie 65 (code ASCII)
# chr(65) renvoie A

def crypteUneLettre(lettre):
    nbr = ord(lettre) - 65
    if (nbr + 3 > 25):
        nbr = (nbr+3)%26
    else :
        nbr = nbr +3
    nbr = nbr +65
    return chr(nbr)
    
def cesar(mot, k):
    for i in mot :
        lettre = mot[i]
        nbr = ord(lettre) - 65
        if (nbr + k > 25):
            nbr = (nbr+3)%26
        else :
            nbr = nbr + k
        nbr = nbr +65
        mot[i] = chr(nbr)
    return mot

def decodeCesar(mot):

