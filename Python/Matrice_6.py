#PIERROT Coralee

def afficher(M):
    for i in range(len(M)):
        print(M[i])


def sommeLig(M,i): 
    s = 0
    for j in range(len(M)): #parcours chaque ligne
        s = s + M[i][j]     #fait la somme des lignes
    return s

def sommeCol(M,j):
    s = 0
    for i in range(len(M)): #parcours les colonnes
        s = s + M[i][j]     #fait la somme des colonnes
    return s

def sommeDiag1(M):
    s = 0
    for i in range(len(M)): #parcours les diagonales
        s = s + M[i][i]     #fait la somme des diagonales
    return s
                       
def sommeDiag2(M):
    s = 0
    for i in range(len(M)):
        s = s + M[len(M)-1-i][i]
    return s

def magic(M):
    for i in range(len(M)): #parcours des i pour les lignes et colonnes
        if(sommeLig(M,i)!=sommeDiag1(M)):  #comparaison avec les lignes
            return False                    #si différent, renvoie faux
        if (sommeCol(M,i)!=sommeDiag1(M)): #comparaison avec les colonnes
            return False
    if(sommeDiag2(M)!=sommeDiag2(M)): #comparaison avec les diagonales 
        return False
    else :
        print(sommeDiag1(M)) #affichage des sommes qui sont toutes égales
        return True         #renvoie vrai
            
##TEST :
##>>> M = [[2,9,4],[7,5,3],[6,1,8]]
##>>> magic(M)
##15
##True
##>>> M = [[0, 1, 2], [0, 1, 2], [0, 1, 2]]
##>>> magic(M)
##False
