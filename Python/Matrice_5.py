
def afficher(M):
    for i in range(len(M)):
        print(M[i])

def init(n):
    return[[0 for i in range(n)] for j in range(n)]

def magic(M):
    n = len(M)
    i = 0
    j = (n-1)//2
    k = 1
    M[i][j]= k
    while(k<n**2):
        diag_i=(i-1)%n
        diag_j=(j+1)%n
        k = k +1
        if(M[diag_i][diag_j]==0):
            i=diag_i
            j=diag_j
        else:
            i = (i+1)%n
        M[i][j]=k
    return M
