#Ecrire les solutions des équations
#du second degré ax² + bx + c = 0

from cmath import *

def secDeg(a,b,c):
    #Calcul de delta
    d = (b*b)-(4*a*c)
    #Dans le cas où delta est positif
    if d > 0:
        x1 = (-b + sqrt(d)) / (2*a)
        x2 = (-b - sqrt(d)) / (2*a)
        print("Les solutions sont x1 =",x1," et x2 =",x2)
    #Dans le cas où delta est négatif
    elif d < 0:
        print("Il n'y a pas de solution")
    #Dans le cas où delta est nul
    else:
        x = -b / (2*a)
        print("La solution est ", x)



def suite(n):
    U=2
    print("U ( 0 ) = ",U)
    for i in range(1, n):
        U = 3*U
        print("U (",i,") = ",U)

