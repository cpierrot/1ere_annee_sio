def afficher(M):
    for i in range(len(M)):
        print (M[i])

def creer(n): #Matrice carr�
    return [[randint (0,9) for i in range(n)] for i in range(n)]

def transposer(M):
    N=[[0 for i in range (len (M))] for j in range(len (M))]
    for i in range(len(M)):
        for j in range(len(M)):
            N[i][j]=M[j][i]
    print("****")
    return N

def produit(M,V): #produit de la matrice M par le vecteur V
#produit([[1,2,3],[-1,0,1],[1,1,1]],[2,5,10]) renvoie le produit de M par V donc [38,8,17]
    P = [0,0,0]
    for i in range(len(M)):
        for j in range(len(M)):
            P[j] = M[i][j]*V[j]
    return P
    
#def ortho(M): # renvoie true si M est orthogonale False sinon
