#Ex1
from random import randint

def mystere():
    nbr = 1 #nbr coups
    m = randint(1,100)#Choix aléatoire d'un nombre compris entre 1 et 100
    n = int(input("Rentrez un nombre entre 1 et 100 : "))
    while n != m: #Tant que le nombre n'a pas été trouvé
        if n < m:#Si le nombre proposé est inférieur au nombre mystère
            print("Le chiffre à trouver est plus grand")
            nbr = nbr +1
        else:#Si le nombre proposé est supérieur au nombre mystère
            print("Le chiffre à trouver est plus petit")
            nbr = nbr +1
        n = int(input("Rentrez un autre nombre : "))
    print("Bravo vous avez gagné ! Le chiffre est bien", m, " wow vous avez gagné en seulement ",nbr,"coups")
                  
#TEST
##>>> mystere()
##Rentrez un nombre entre 1 et 99
##50
##Le chiffre est plus grand
##55
##Le chiffre est plus grand
##80
##Le chiffre est plus petit
##70
##Le chiffre est plus grand
##75
##Le chiffre est plus petit
##73
##Bravo bous avez gagné ! Le chiffre est bien 73


#Ex2
def entier():
    n = input('Saisissez un entier naturel : ')
    n = int(n)
    print("La somme est",n *((1+n)/2))


##TEST
##>>> entier()
##Saisissez un entier naturel : 7
##La somme est 28.0
##>>> entier()
##Saisissez un entier naturel : 10
##La somme est 55.0
##>>> entier()
##Saisissez un entier naturel : 4
##La somme est 10.0
