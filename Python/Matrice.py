from random import randint

def afficher(A):
    for i in range(len(A)):
        print (A[i])

        
#Exercice 1 : écrire une fonction qui renvoie une liste nulle de taille n
            #ex : init1(5) renvoie [0,0,0,0,0]
def init1(n):
    A = []
    for i in range(n):
        A = A + [0]
    return A

def init2(n):
    return [0 for i in range(n)]

def init3(n):
    return [randint(0,1) for i in range(n)]

#Exercice 2 : écrire une fonction qui renvoie une matrice carrée nulle d'ordre n

def init4(n):
    return[[randint(0,1) for i in range(n)] for j in range(n)]

def luminosité(n):
    res = 0
    A = init4(n)
    for i in range(n):
        for j in range(n):
            res = res + A[i][j]
    print (A)
    return res

#procédure qui permet d'afficher une matrice
def afficher(M):     
    for i in range(len(M)): # len(M) est le nombre de ligne, len(M[0]) est le nombre de colonne
        print(M[i])
    return

#initialiser une matrice nulle
def init(n):
    return [[0 for i in range (n)] for j in range(n)]

##>>> init(10)
##[[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
##>>> afficher(init(10))
##[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
##[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
##[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
##[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
##[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
##[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
##[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
##[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
##[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
##[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

#faire une matrice identité
def identite(M):
    n = init(M)
    for i in range(len(n)):
        n[i][i] = 1
    return n
    
#créer matrice croix
def croix(M):
    n = init(M)
    d = len(n) -1
    for i in range(len(n)):
        n[i][i] = 1
        n[i][d] = 1
        d = d-1
    return n
        
#sur les bord mettre que des 1 : cadre
def bord(M):
    n = init(M)
    for i in range(M):
        n[0][i] = 1
        n[i][0] = 1
        n[i][M-1] = 1
        n[M-1][i] = 1
    return n
    
def pixel(i,j):
    M = init(10)
    M[i][j] = 1
    return M

def boat(n):
    M = init(n)
    x = randint(0,n-2)
    M[1][x] = 1
    M[1][x+1] = 1
    return M
    
    
