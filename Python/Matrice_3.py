from random import randint
def afficher(M):#M est une matrice
    for i in range(len(M)):
        print (M[i])

def init(n): #renvoie une matrice carrée d'ordre n
    return[[randint(0,1) for i in range(n)] for j in range(n)]

def init2(n,m):#renvoie une matrice rectangulaire
    return[[randint(0,1) for i in range(n)] for j in range(m)]


def align(L): #prend un argument une liste L aléatoirement remplie de 0 et 1
    #et renvoie True si L est remplie uniquement de 1
    for i in range(len(L[0])):
        for j in range(len(L)):
            if L[i][j]==0:
                return False
    return True

def alignement(n): #renvoie le nombre de lignes complètes de la matrice carrée
    M = init(n)
    afficher(M)
    compte = 0
    boule = True
    for j in range(n):
        for i in range(n):
                if M[j][i] == 0:
                    boule = False
        if boule == True:
            compte = compte +1
        boule = True
    for j in range(n):
        for i in range(n):
                if M[i][j] == 0:
                    boule = False
        if boule == True:
            compte = compte +1
        boule = True
                
    return compte
