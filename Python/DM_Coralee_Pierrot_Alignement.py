#DM PIERROT Coralee SIO 1b
def afficher(M):#M est une matrice
    for i in range(len(M)):
        print (M[i])
    
def alignement(n): #renvoie le nombre de lignes et colonnes complètes de la matrice carrée
    M = init(n)
    afficher(M)    #affichage de la matrice créée
    compte = 0     #initialisation du compteur à 0
    boule = True   #initialisation de la variable booléenne
    for j in range(n):
        for i in range(n):
                if M[j][i] == 0:
                    boule = False   #On met la variable à false si la ligne possède un 0
        if boule == True:           #Si aucun 0, alors on incrémente le compteur
            compte = compte +1
        boule = True                #réinitialisation de la variable booléenne
    for j in range(n):
        for i in range(n):
                if M[i][j] == 0:
                    boule = False   #On met la variable à false si la colonne possède un 0
        if boule == True:           #Si aucun 0, alors on incrémente le compteur
            compte = compte +1
        boule = True                #réinitialisation de la variable booléenne
                
    return compte


##Test :
##>>> alignement(3)
##[1, 1, 1]
##[1, 0, 0]
##[0, 0, 0]
##1
##>>> alignement(3)
##[1, 1, 0]
##[1, 0, 1]
##[0, 1, 1]
##0
##>>> alignement(3)
##[1, 0, 1]
##[1, 1, 1]
##[1, 0, 0]
##2
