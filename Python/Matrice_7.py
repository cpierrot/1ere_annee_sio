from random import randint

def afficher(M):
    for i in range(len(M)):
        print(M[i])
        
def init(n,m):
    return[[0 for i in range(m)] for j in range(n)]

def galton(essai):# matrice nulle d'ordre 7*13
    L = [0 for col in range(13)] #création liste de comptage
    for j in range(essai): #Boucle sur le nombre d'essai choisi
        M = [[0 for col in range(13)] for lig in range(7)] #initialisation de la matrice nulle
        M[0][6] = 1
        col = 6
        i = 1
        for i in range(1, len(M)): #Boucle jusqu'à la fin de la matrice
            if randint(0,1)==1: #la boule part à droite
                col = col+1
                M[i][col]=1
            else :
                 col = col - 1 #la boule part à gauche
                 M[i][col]=1
        L[col]+=1 #Arrivé à la fin de la matrice, on incrémente de 1 sur l'endroit de la liste
    afficher(M)
    print('......................................')
    print(L)
    return 
