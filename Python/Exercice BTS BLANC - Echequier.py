def init():
    return [[0 for col in range(8)] for lig in range(8)]

def afficher(M):
    for i in range(8):
        print(M[i])

def tour(lig,col):
    M=init()
    for i in range(len(M)):
        for j in range(len(M)):
            if i==lig or j==col:
                M[i][j] = 1
    M[lig][col]=2
    return afficher(M)

def fou(lig,col):
    M=init()
    for i in range(len(M)):
        for j in range(len(M)):
            if i+j == lig+col or i-j == lig-col:
                M[i][j]=1
    M[lig][col]=2
    return afficher(M)

def cavalier(lig,col):
    M=init()
    if (lig+2<=7): # carre bas 
        if (col+1<=7):              
              M[lig+2][col+1] = 1
        if (col-1>=0):
             M[lig+2][col-1] = 1
    if (col+2<=7):
        if (lig+1<=7):
            M[lig+1][col+2] = 1 # carre droite
        if (lig-1>=0):
            M[lig-1][col+2] = 1
    if (col-2>=0):
        if (lig-1>=0):
            M[lig-1][col-2] = 1 # carre gauche
        if (lig+1<=7):
            M[lig+1][col-2] = 1
    if (lig-2>=0):
        if (col+1<=7):
            M[lig-2][col+1] = 1 # carre haut
        if (col-1>=0):
            M[lig-2][col-1] = 1
    M[lig][col] = 2
    return afficher(M)

def reine(lig,col):
    M = init()
    for i in range(8):
        M[lig][i] = 1
        M[i][col] = 1
    for i in range (8):
        for j in range(8):
            if (i+j == lig+col):
                M[i][j] = 1
            if (i-j == lig-col):
                M[i][j] = 1
    M[lig][col] = 2
    return afficher(M)

