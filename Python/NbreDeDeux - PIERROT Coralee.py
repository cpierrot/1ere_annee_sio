#nbreDeDeux(12) renvoie 2 car 12 = 2*2*3
#nbreDeDeux(24) renvoie 3 car 24 = 2*2*2*3
#nbreDeDeux(30) renvoie 1 car 30 = 2*3*3
#nbreDeDeux(25) renvoie 0 car 25 = 5*5

def nbreDeDeux(n):
    s = 0
    while n%2==0:
        s = s+1
        n = n /2
    return s
