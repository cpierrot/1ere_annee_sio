from random import *

def afficher(M):
    for i in range(len(M)):
        print(M[i])

def init(n):
    return[[randint(0,1) for i in range(n)] for j in range(n)]

def init0(n):
    return[[0 for i in range(n)] for j in range(n)]

def pascal(n):
    M = init0(n)
    for i in range(n):
        M[i][0] = 1
        for j in range(1,n):
            M[i][j] = M[i-1][j]+M[i-1][j-1]
    return M

def sommeLigne(n):
    M = pascal(n)
    N=[]
    somme = 0
    for i in range(1,n):
        for j in range(n):
            somme = somme + M[i][j]
        N= N +[somme]
    return N

def fibonacci(n):
    return
